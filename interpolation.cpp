#include <iostream>
#include <fstream>
#include <cmath>

#include "src/fem/Domain.h"
#include "src/fem/FEMNode.h"
#include "src/fem/FEMQuad.h"
#include "src/fem/Analysis/StaticAnalysis.h"
#include "src/fem/Analysis/DynamicAnalysis.h"
#include "src/fem/Solver/StaticSolver.h"
#include "src/fem/Solver/NewmarkSolver.h"


#include "src/mesh/Mesh.h"
#include "src/interpolationMethod/idw.h"
#include "src/interpolationMethod/idwMod.h"
#include "src/interpolationMethod/rbf.h"

#include "src/utilities/utilities.h"

using namespace fem;
using namespace im;
using namespace mesh;
using namespace utilities;
using namespace std;

void rotationTest();

void movingTest();

void cylinderTest();

void interpolationTest();

void fsiTest();

void femTest();

void femDynamicTest();

int num_threads = 1;

int main()
{
	cout << "Start the program" << endl << endl;

	try
	{
		femDynamicTest();
//		femTest();
	}
	catch (const std::exception& e)
	{
		 std::cout << e.what();
		 return 1;
	}

	cout << endl << "Finish the program" << endl;

	return 0;
}

void fsiTest()
{
	Mesh *fluid = new Mesh();
	fluid->importVTU("fluid");

	Mesh *structure = new Mesh();
	structure->importKIM("struct.kim");

	std::vector<std::vector<node*> > borderS(3);
	std::vector<std::vector<node*> > borderF(3);
	std::vector<std::vector<node*> > innerF(3);

	for (node* n: structure->getNodes())
	{
		double y = n->y() - 0.2;
		double z = n->z() - 0.2;

//		double r = y * y + z * z - 0.05 * 0.05;

//		if ( fabs(r) < 1e-8 )
		{
			borderS[0].push_back(n);
			borderS[1].push_back(n);
			borderS[2].push_back(n);
		}
	}

	for (node* n: fluid->getNodes())
	{
		double y = n->y();
		double z = n->z();

		double r = (y - 0.2) * (y - 0.2) + (z - 0.2) * (z - 0.2) - 0.05 * 0.05;

		if ( fabs(r) < 1e-8 )
		{
			borderF[0].push_back(n);
			borderF[1].push_back(n);
			borderF[2].push_back(n);
		}
	}

	idw *IDW = new idw(3);
	rbf *RBF= new rbf();
	idwMod *IDWMOD = new idwMod(3);

	system::Clock_t start = system::Clock();
	RBF->deformation(borderS, borderF);
	cout << endl << "Border struct to border fluid: " << system::Clock() - start << endl << endl;

	fluid->export2vtu("fsi_fluid_b");

	for (node* n: fluid->getNodes())
	{
		double x = n->x();
		double y = n->y();
		double z = n->z();

		if ( y > 0.0 && y < 2.2 && z > 0.0 && z < 0.41 )
		{
			innerF[0].push_back(n);
			innerF[1].push_back(n);
			innerF[2].push_back(n);
		}
		else if ( (y == 0.0 || y == 2.2)  && z != 0.0 && z != 0.41 )
		{
			n->dxdydz(0, 0.0);
			n->dxdydz(1, 0.0);
			borderF[0].push_back(n);
			borderF[1].push_back(n);
//			borderS[0].push_back(n);
//			borderS[1].push_back(n);
			innerF[2].push_back(n);
		}
		else if ( (z == 0.0 || z == 0.41)  && y != 0.0 && y != 2.2 )
		{
			n->dxdydz(0, 0.0);
			n->dxdydz(2, 0.0);
			borderF[0].push_back(n);
			borderF[2].push_back(n);
//			borderS[0].push_back(n);
//			borderS[2].push_back(n);
			innerF[1].push_back(n);
		}
		else
		{
			n->dxdydz(0.0, 0.0, 0.0);
			borderF[0].push_back(n);
			borderF[1].push_back(n);
			borderF[2].push_back(n);
//			borderS[0].push_back(n);
//			borderS[1].push_back(n);
//			borderS[2].push_back(n);
		}
	}

	start = system::Clock();
	IDWMOD->deformation(borderF, innerF);
	cout << endl << "Border fluid to inner fluid: " << system::Clock() - start << endl << endl;

	fluid->export2vtu("fsi_fluid");
	structure->export2vtu("fsi_struct");
}

void interpolationTest()
{
	cout << "Initialize mesh\n";
	Mesh *aMesh = new Mesh();

	cout << "Import mesh \n";
	aMesh->importMSH("data.msh");

	std::vector<node*> Left;
	std::vector<node*> Right;
	for (node* n: aMesh->getNodes())
	{
		if ( n->x() >= 0.25 && n->x() <= 0.75 && n->y() >= 0.45 && n->y() <= 0.55 )
		{
			n->data(100 * (n->x() - 0.5));
			Left.push_back(n);
		}
		else
		{
			Right.push_back(n);
		}
	}
	cout << "Export init mesh \n";
	aMesh->export2vtu("init");

//	idw *IDW = new idw(2);
	rbf *RBF = new rbf();
	//idwMod *IDWMOD = new idwMod();

//	IDW->interpolate(Left, Right);
	RBF->interpolate(Left, Right);
	//IDWMOD->interpolate(Left, Right);
	aMesh->export2vtu("result");

	delete aMesh;
}

void rotationTest()
{
	cout << "Initialize mesh\n";
	Mesh *aMesh = new Mesh();

	cout << "Import mesh \n";
	aMesh->importMSH("data.msh");

	std::vector<std::vector<node*> > Left(2);
	std::vector<std::vector<node*> > Right(2);

	double fi = M_PI / 4.0;

	for (node* n: aMesh->getNodes())
	{
		double x = n->x();
		double y = n->y();

		if ( x >= 0.3 && x <= 0.7 && y >= 0.45 && y <= 0.55 )
		{
			double aX = n->x();
			double aY = n->y();

			double dx = (aX - 0.5) * cos(fi) + (0.5 - aY) * sin(fi) + 0.5 - aX;
			double dy = (aY - 0.5) * cos(fi) + (aX - 0.5) * sin(fi) + 0.5 - aY;

			n->dxdydz(dx, dy, 0.0);
			Left[0].push_back(n);
			Left[1].push_back(n);
		}
		else if ( x > 0.0 && x < 1.0 && y > 0.0 && y < 1.0 )
		{
			Right[0].push_back(n);
			Right[1].push_back(n);
		}
		else if ( ( x == 0.0 || x == 1.0 ) && y != 0.0 && y != 1.0 )
		{
			n->dxdydz(0, 0.0);
			Left[0].push_back(n);

			Right[1].push_back(n);
		}
		else if ( ( y == 0.0 || y == 1.0 ) && x != 0.0 && x != 1.0 )
		{
			n->dxdydz(1, 0.0);
			Left[1].push_back(n);

			Right[0].push_back(n);
		}
		else
		{
			n->dxdydz(0.0, 0.0, 0.0);
			Left[0].push_back(n);
			Left[1].push_back(n);
		}
	}
	cout << "Export init mesh \n";
	aMesh->export2vtu("init");


	idw *IDW = new idw(1);
	rbf *RBF= new rbf();
//	idwMod *IDWMOD = new idwMod(4);

	system::Clock_t start = system::Clock();
//	RBF->deformation(Left, Right);

	IDW->deformation(Left, Right);
//	IDWMOD->deformation(Left, Right);

	cout << endl << system::Clock() - start << endl << endl;

	aMesh->export2vtu("rotationTest");

	delete aMesh;
}

void movingTest()
{
	cout << "Initialize mesh\n";
	Mesh *aMesh = new Mesh();

	cout << "Import mesh \n";
	aMesh->importMSH("data.msh");

	std::vector<std::vector<node*> > Left(2);
	std::vector<std::vector<node*> > Right(2);

	for (node* n: aMesh->getNodes())
	{
		double x = n->x();
		double y = n->y();

		if ( x >= 0.25 && x <= 0.75 && y >= 0.45 && y <= 0.55 )
		{
			double dx = 0.1;
			double dy = 0.1;

			n->dxdydz(dx, dy, 0.0);
			Left[0].push_back(n);
			Left[1].push_back(n);
		}
		else if ( x > 0.0 && x < 1.0 && y > 0.0 && y < 1.0 )
		{
			Right[0].push_back(n);
			Right[1].push_back(n);
		}
		else if ( ( x == 0.0 || x == 1.0 ) && y != 0.0 && y != 1.0 )
		{
			n->dxdydz(0, 0.0);
			Left[0].push_back(n);

			Right[1].push_back(n);
		}
		else if ( ( y == 0.0 || y == 1.0 ) && x != 0.0 && x != 1.0 )
		{
			n->dxdydz(1, 0.0);
			Left[1].push_back(n);

			Right[0].push_back(n);
		}
		else
		{
			n->dxdydz(0.0, 0.0, 0.0);
			Left[0].push_back(n);
			Left[1].push_back(n);
		}
	}
	cout << "Export init mesh \n";
	aMesh->export2vtu("init");

//	idw *IDW = new idw(1);
	rbf *RBF= new rbf();
//	idwMod *IDWMOD = new idwMod(4);

	system::Clock_t start = system::Clock();

	RBF->deformation(Left, Right);
//	IDW->deformation(Left, Right);
//	IDWMOD->deformation(Left, Right);

	cout << endl << system::Clock() - start << endl << endl;

	aMesh->export2vtu("movingTest");

	delete aMesh;
}

void cylinderTest()
{
	Mesh *fluid = new Mesh();
	fluid->importVTU("fluid");

	Mesh *structure = new Mesh();
	structure->importVTU("struct");

	std::vector<std::vector<node*> > borderS(3);
	std::vector<std::vector<node*> > borderF(3);
	std::vector<std::vector<node*> > innerF(3);

	double l = 0.025;
	for (node* n: structure->getNodes())
	{
		double x = n->x();
		double y = n->y();
		double z = n->z();

		double dr = y * y + z * z - 0.005 * 0.005;

		if ( (x == 0 || x == 0.05) && fabs(dr) < 1e-8  )
		{
			n->dxdydz(0.0, 0.0, 0.0);
			borderS[0].push_back(n);
			borderS[1].push_back(n);
			borderS[2].push_back(n);
		}

		if ( (x >= 0.025 - l * 0.5) && (x <= 0.025 + l * 0.5) )
		{
//			double dr = 2e-3 * sin(M_PI / l * (x - 0.025 + l * 0.5));

			double dr = 1e-2 * sin(M_PI / l * (x - 0.025 + l * 0.5));

			double r = sqrt( y * y + z * z);
			double phi = asin(z / r);

			if ( y >= 0 && z >= 0)
			{
				n->dxdydz(0.0, dr * cos(phi), dr * sin(phi));
//				cout << y << " " << z << " " << phi / M_PI * 180 << endl;
			}
			else if ( y < 0 && z >= 0 )
			{
				n->dxdydz(0.0, -dr * cos(phi), dr * sin(phi));
//				cout << y << " " << z << " " << phi / M_PI * 180<< endl;
			}
			else if ( y < 0 && z < 0 )
			{
				n->dxdydz(0.0, -dr * cos(phi), dr * sin(phi));
//				cout << y << " " << z << " " << phi / M_PI * 180<< endl;
			}
			else if ( y > 0 && z < 0 )
			{
				n->dxdydz(0.0, dr * cos(phi), dr * sin(phi));
//				cout << y << " " << z << " " << phi / M_PI * 180<< endl;
			}

			borderS[0].push_back(n);
			borderS[1].push_back(n);
			borderS[2].push_back(n);
		}
		else
		{
			n->dxdydz(0.0, 0.0, 0.0);
			borderS[0].push_back(n);
			borderS[1].push_back(n);
			borderS[2].push_back(n);

		}
	}

	for (node* n: fluid->getNodes())
	{
		double y = n->y();
		double z = n->z();

		double dr = y * y + z * z - 0.005 * 0.005;

		if ( fabs(dr) < 1e-8  )
		{
			n->dxdydz(0.0, 0.0, 0.0);
			borderF[0].push_back(n);
			borderF[1].push_back(n);
			borderF[2].push_back(n);
		}
		else
		{
			n->dxdydz(0.0, 0.0, 0.0);
			innerF[0].push_back(n);
			innerF[1].push_back(n);
			innerF[2].push_back(n);
		}
	}

//	idw *IDW = new idw(2);
	rbf *RBF= new rbf();
//	idwMod *IDWMOD = new idwMod(4);

	system::Clock_t start = system::Clock();

	RBF->deformation(borderS, borderF);
	RBF->deformation(borderF, innerF);

	cout << endl << system::Clock() - start << endl << endl;

	fluid->export2vtu("fluid_result");
	structure->export2vtu("structure_result");
}

void femTest()
{
	Domain *aMesh = new Domain();

	cout << "Import domain\n";
//	aMesh->importGMSH("cyl.mesh");
//	aMesh->importGMSH("lame.mesh");
//	aMesh->importGMSH("4quad.mesh");
	aMesh->importGMSH("chek.mesh");

	/************** Start of preparing Mesh *******************/
	cout << endl << "Start preparing Mesh" << endl;

	cout << "Add material \n";
	for (primitiveElement* e : aMesh->getElements())
	{
		FEMQuad* fq = dynamic_cast<FEMQuad*>(e);

		fq->addMaterial(210e9, 0.3);
		// Unit Test
//		fq->addMaterial(1, 0.0);
	}

	cout << "Add constraints \n";
	for (node* n : aMesh->getNodes())
	{
		FEMNode* fn = dynamic_cast<FEMNode*>(n);

		// Unit Test
//		if ( fn->x() == 0.0 )
//		{
//			std::vector<std::pair<int, double> > cnstr;
//
//			cnstr.push_back(std::make_pair(0, 0.0));
//
//			fn->addConstraint(cnstr);
//		}

//		// Lame Test
//		if ( fn->x() == 0.0)
//		{
//			std::vector<std::pair<int, double> > cnstr;
//
//			cnstr.push_back(std::make_pair(0, 0.0));
//
//			fn->addConstraint(cnstr);
//		}
//		else if ( fn->y() == 0.0 )
//		{
//			std::vector<std::pair<int, double> > cnstr;
//
//			cnstr.push_back(std::make_pair(1, 0.0));
//
//			fn->addConstraint(cnstr);
//		}

		// Test
		if ( fn->x() == 0.1 )
		{
			std::vector<std::pair<int, double> > cnstr;

			cnstr.push_back(std::make_pair(0, 0.0));
			cnstr.push_back(std::make_pair(1, 0.0));

			fn->addConstraint(cnstr);
		}
//		else if ( fn->y() == 0.0 )
		else if ( fn->x() == 0.0 )
		{
			std::vector<std::pair<int, double> > cnstr;

			cnstr.push_back(std::make_pair(0, 0.0));
			cnstr.push_back(std::make_pair(1, 0));

			fn->addConstraint(cnstr);
		}
	}

	cout << "Add pressure \n";
	for (primitiveElement* e : aMesh->getElements())
	{
		FEMQuad* fq = dynamic_cast<FEMQuad*>(e);

		FEMNode *a, *b;
		int flag = 0;
		for (node* n : fq->getNodes())
		{
			double x = n->x();
			double y = n->y();
			// Unit Test
//			if ( fabs(n->x() - 2.0) < 1e-6)
			// Lame Test
//			if ( fabs(x * x + y * y  - 0.1 * 0.1) < 1e-6)
			// Test
			if ( fabs(n->y() - 0.01) < 1e-6 && n->x() > 0.0399 && n->x() < 0.0601 )
//			if ( fabs(n->y() - 0.02) < 1e-6 && n->x() < 0.02 + 1e-6 )
			{
				if ( flag == 0 )
					a = dynamic_cast<FEMNode*>(n);
				else if (flag == 1)
					b = dynamic_cast<FEMNode*>(n);
				flag++;
			}
			if (flag == 2)
				break;
		}

		if (flag == 2)
			fq->addLoad(a, b, -17e7);
	}

	cout << "Export init mesh\n";
	aMesh->export2vtu("init");

	cout << "End preparing Mesh" << endl << endl;
	/************** END of preparing Mesh *******************/


	/************** Start of ANALYSIS ***********************/
	cout << endl << "Start Analysis" << endl;

	solver::Solver* solver = new solver::StaticSolver(1e-6, 20000);

	analysis::Analysis* analysis = new analysis::StaticAnalysis(aMesh, solver);

	analysis->Analyze();

	cout << "End Analysis" << endl << endl;
	/************** End of ANALYSIS ***********************/

	cout << "Export result mesh" << endl;
	aMesh->export2vtu("result_static");
}

void femDynamicTest()
{
	Domain *aMesh = new Domain();

	cout << "Import domain\n";
//	aMesh->importGMSH("chek.mesh");
	aMesh->importGMSH("cyl.mesh");

	/************** Start of preparing Mesh *******************/
	cout << endl << "Start preparing Mesh" << endl;

	cout << "Add material \n";
	for (primitiveElement* e : aMesh->getElements())
	{
		FEMQuad* fq = dynamic_cast<FEMQuad*>(e);

		fq->addMaterial(210e9, 0.3);
		fq->addDensity(7800);
	}

	cout << "Add constraints \n";
	for (node* n : aMesh->getNodes())
	{
		FEMNode* fn = dynamic_cast<FEMNode*>(n);

		// Unit Test
//		if ( fn->x() == 0.0 )
//		{
//			std::vector<std::pair<int, double> > cnstr;
//
//			cnstr.push_back(std::make_pair(0, 0.0));
//
//			fn->addConstraint(cnstr);
//		}

		if ( fn->x() == 0.1 )
		{
			std::vector<std::pair<int, double> > cnstr;

			cnstr.push_back(std::make_pair(0, 0.0));
			cnstr.push_back(std::make_pair(1, 0.0));

			fn->addConstraint(cnstr);
		}
		else if ( fn->y() == 0.0 )
//		else if ( fn->x() == 0.0 )
		{
			std::vector<std::pair<int, double> > cnstr;

//			cnstr.push_back(std::make_pair(0, 0.0));
			cnstr.push_back(std::make_pair(1, 0.0));

			fn->addConstraint(cnstr);
		}
	}

	cout << "Add pressure \n";
	for (primitiveElement* e : aMesh->getElements())
	{
		FEMQuad* fq = dynamic_cast<FEMQuad*>(e);

		FEMNode *a, *b;
		int flag = 0;
		for (node* n : fq->getNodes())
		{
//			if ( fabs(n->y() - 0.01) < 1e-6 && n->x() > 0.0399 && n->x() < 0.0601 )
			if ( fabs(n->y() - 0.02) < 1e-6 && n->x() < 0.05 )
			{
				if ( flag == 0 )
					a = dynamic_cast<FEMNode*>(n);
				else if (flag == 1)
					b = dynamic_cast<FEMNode*>(n);
				flag++;
			}
			if (flag == 2)
				break;
		}

		if (flag == 2)
			if (a->x() < 0.005 && b->x() < 0.005 )
				fq->addLoad(a, b, -17e7);
			else
				fq->addLoad(a, b, 0);
	}

	cout << "Export init mesh\n";
	aMesh->export2vtu("init");

	cout << "End preparing Mesh" << endl << endl;
	/************** END of preparing Mesh *******************/


	/************** Start of ANALYSIS ***********************/
	cout << endl << "Start Analysis" << endl;

	solver::Solver* solver = new solver::NewmarkSolver(1e-6, 0.5);

	analysis::Analysis* analysis = new analysis::DynamicAnalysis(aMesh, solver, 1000);

	analysis->Analyze();

	cout << "End Analysis" << endl << endl;
	/************** End of ANALYSIS ***********************/
}
