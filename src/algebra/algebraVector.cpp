#include "algebraVector.h"

#include <stdexcept>
#include <cmath>

namespace algebra
{
	algebraVector::algebraVector(uint size)
	{
		_data.resize(size);
	}

	algebraVector::algebraVector(const algebraVector &vector)
	{
		_data = vector.getData();
	}

	algebraVector::algebraVector(const std::vector<double> &vector)
	{
		_data = vector;
	}

	double& algebraVector::operator()(uint index)
	{
		return _data[index];
	}

	const double algebraVector::operator()(uint index) const
	{
		return _data[index];
	}

	algebraVector& algebraVector::operator=(const algebraVector &vector)
	{
		if ( Size() == vector.Size() )
			_data = vector.getData();
		else
			throw std::logic_error("Error: Size mismatch in algebraVector& algebraVector::operator= (const algebraVector &vector)");

		return *this;
	}

	algebraVector& algebraVector::operator=(const double &value)
	{
		for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++)
			*iter = value;

		return *this;
	}

	algebraVector& algebraVector::operator+=(const algebraVector &vector)
	{
		if ( Size() == vector.Size() )
		{
			uint i = 0;
			for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++, i++)
				*iter += vector(i);
		}
		else
			throw std::logic_error("Error: Size mismatch in algebraVector& algebraVector::operator+= (const algebraVector &vector)");

		return *this;
	}

	algebraVector& algebraVector::operator+=(const double value)
	{
		for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++)
			*iter += value;

		return *this;
	}

	algebraVector& algebraVector::operator-=(const algebraVector &vector)
	{
		if ( Size() == vector.Size() )
		{
			uint i = 0;
			for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++, i++)
				*iter -= vector(i);
		}
		else
			throw std::logic_error("Error: Size mismatch in algebraVector& algebraVector::operator-= (const algebraVector &vector)");

		return *this;
	}

	algebraVector& algebraVector::operator-=(const double value)
	{
		for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++)
			*iter -= value;

		return *this;
	}

	algebraVector& algebraVector::operator*=(const double value)
	{
		for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++)
			*iter *= value;

		return *this;
	}

	algebraVector& algebraVector::operator/=(const double value)
	{
		if (value)
			for (std::vector<double>::iterator iter = _data.begin(); iter != _data.end(); iter++)
				*iter /= value;
		else
			throw std::logic_error("Error: Division by zero in algebraVector& algebraVector::operator/= (const algebraVector &vector)");

		return *this;
	}

	void algebraVector::Resize(int newsize)
	{
		if (newsize >= 0)
			_data.resize(newsize);
		else
			throw std::logic_error("Error: Negative size in algebraVector& algebraVector::Resize(int newsize)");
	}

	void algebraVector::Zero()
	{
		_data.clear();
		std::vector<double>(_data.capacity()).swap(_data);
	}

	double algebraVector::Norm() const
	{
		double norm = 0.0;

		for (std::vector<double>::const_iterator iter = _data.begin(); iter != _data.end(); iter++)
			norm += *iter * *iter;

		return sqrt(norm);
	}

	algebraVector algebraVector::Normalize() const
	{
		algebraVector result(*this);
		double norm = result.Norm();
		if (norm)
			result /= norm;

		return result;
	}

	algebraVector algebraVector::operator+(const algebraVector &a)
	{
		algebraVector result(*this);
		result += a;

		return result;
	}

	algebraVector algebraVector::operator+(const double value)
	{
		algebraVector result(*this);
		result += value;

		return result;
	}

	algebraVector algebraVector::operator-(const algebraVector &a)
	{
		algebraVector result(*this);
		result -= a;

		return result;
	}

	algebraVector algebraVector::operator-(const double value)
	{
		algebraVector result(*this);
		result -= value;

		return result;
	}

	algebraVector algebraVector::operator*(const double value) const
	{
		algebraVector result(*this);
		result *= value;

		return result;
	}

	double algebraVector::operator*(const algebraVector &b) const
	{
		uint bsize = b.Size();
		if (Size() == bsize)
		{
			double result = 0;
			for (uint i = 0; i < bsize; i++)
				result += (*this)(i) * b(i);

			return result;
		}
		else
			throw std::logic_error("Error: algebraVector::operator* (algebraVector &) : Sizes Mismatch");
	}

	algebraVector algebraVector::operator*(const algebraMatrix &M)
	{
		uint rows = M.Rows();
		uint cols = M.Cols();
		if (Size() == rows)
		{
			algebraVector result(cols);
			for (uint i = 0; i < cols; i++)
			{
				double sum = 0;
				for (uint j = 0; j < rows; j++)
					sum += (*this)(j) * M(j, i);

				result(i) = sum;
			}
			return result;
		}
		else
			throw std::logic_error("Error: algebraVector algebraVector::operator*(const AlgebraMatrix &M) : Sizes Mismatch");
	}

	algebraVector algebraVector::ProductSchur(const algebraVector &b)
	{
		if (Size() == b.Size())
		{
			algebraVector result(Size());
			for (uint i = 0; i < Size(); i++)
			{
				result(i) = (*this)(i) * b(i);
			}
			return result;
		}
		else
			throw std::logic_error("Error: algebraVector algebraVector::operator*(const AlgebraMatrix &M) : Sizes Mismatch");
	}

	algebraVector algebraVector::operator/(const double value)
	{
		if (value)
		{
			algebraVector result(*this);
			result /= value;
			return result;
		}
		else
			throw std::logic_error("Error: algebraVector::operator/ (double) : Divide by Zero");
	}

	bool algebraVector::operator==(const algebraVector& rhs) const
	{
		bool flag = true;
		if (Size() == rhs.Size())
			for (uint i = 0; i < Size(); i++)
				if ((*this)(i) != rhs(i))
				{
					flag = false;
					break;
				}
		else
			flag = false;

		return flag;
	}

	bool algebraVector::operator!=(const algebraVector& rhs) const
	{
		return !operator==(rhs);
	}

	algebraVector algebraVector::operator-() const
	{
		algebraVector res(Size());
		for (uint i = 0; i < Size(); i++)
			res(i) = -(*this)(i);

		return res;
	}

	double algebraVector::Average()
	{
		double result = 0.0;
		for (std::vector<double>::const_iterator iter = _data.begin(); iter != _data.end(); iter++)
			result += *iter;

		return result / Size();
	}

	std::ostream& operator<<(std::ostream& Stream, const algebraVector& rhs)
	{
		Stream.flags(std::ios::scientific | std::ios::left | std::ios::dec);
		Stream.precision(7);

		Stream << "(";

		int size = rhs.Size();
		for (int i = 0; i < size; i++)
		{
			if ( fabs(rhs(i)) < 1e-15 )
				Stream << 0.0;
			else
				Stream << rhs(i);

			if (i != size - 1)
				Stream << ", ";
		}
		Stream << ")\n";

		return Stream;
	}

	algebraVector operator*(const double lhs, const algebraVector& rhs)
	{
		return rhs * lhs;
	}
}
