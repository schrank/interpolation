#ifndef SRC_ALGEBRA_ALGEBRAVECTOR_H_
#define SRC_ALGEBRA_ALGEBRAVECTOR_H_

#include <vector>
#include <iostream>

#include "algebraMatrix.h"

namespace algebra
{

class algebraMatrix;

class algebraVector
{
public:
	algebraVector() {}

	algebraVector(uint size);

	algebraVector(const algebraVector &vector);

	algebraVector(const std::vector<double> &vector);

	virtual ~algebraVector() {
		_data.resize(0);
	}

	inline uint Size() const {
		return _data.size();
	}

	inline bool Empty() const {
		return _data.empty();
	}

	inline std::vector<double> getData() const {
		return _data;
	}

	double& operator()(uint index);

	const double operator()(uint index) const;

	algebraVector& operator=(const algebraVector &vector);

	algebraVector& operator=(const double &value);

	algebraVector& operator+=(const algebraVector &vector);

	algebraVector& operator+=(const double);

	algebraVector& operator-=(const algebraVector &vector);

	algebraVector& operator-=(const double value);

	algebraVector& operator*=(const double value);

	algebraVector& operator/=(const double value);

	algebraVector operator+(const algebraVector &a);

	algebraVector operator+(const double value);

	algebraVector operator-(const algebraVector &a);

	algebraVector operator-(const double value);

	algebraVector operator*(const double value) const;

	algebraVector operator*(const algebra::algebraMatrix &M);

	double operator*(const algebraVector &b) const;

	algebraVector ProductSchur(const algebra::algebraVector &b);

	algebraVector operator/(const double value);

	algebraVector operator-() const;

	bool operator==(const algebraVector& rhs) const;

	bool operator!=(const algebraVector& rhs) const;

	virtual void Resize(int newsize);

	virtual void Zero();

	virtual double Norm() const;

	virtual algebraVector Normalize() const;

	virtual double Average();

	friend std::ostream& operator<<(std::ostream& Stream, const algebra::algebraVector& rhs);

	friend algebraVector operator*(const double value, const algebraVector& rhs);

private:
	std::vector<double> _data;
};

std::ostream& operator<<(std::ostream& Stream, const algebra::algebraVector& rhs);

algebraVector operator*(const double value, const algebraVector& rhs);

} /* namespace algebra */

#endif /* SRC_ALGEBRA_ALGEBRAVECTOR_H_ */
