#ifndef SRC_ALGEBRA_ALGEBRAMATRIX_H_
#define SRC_ALGEBRA_ALGEBRAMATRIX_H_

typedef unsigned int uint;

#include <vector>
#include <iostream>

#include "algebraVector.h"

namespace algebra
{

class algebraVector;

class algebraMatrix
{
public:
	algebraMatrix(): _rows(0), _cols(0) {}

	algebraMatrix(uint rows, uint cols);

	algebraMatrix(const algebraMatrix &algebraMatrix);

	virtual ~algebraMatrix() {
		for ( std::vector <double> vec : _data)
			vec.clear();

		_data.clear();
	}

	virtual double& operator()(uint row, uint col);

	virtual const double& operator()(uint row, uint col) const;

	virtual algebraMatrix& operator=(const algebraMatrix &matrix);

	virtual algebraMatrix& operator+=(const algebraMatrix &algebraMatrix);

	virtual algebraMatrix& operator-=(const algebraMatrix &algebraMatrix);

	virtual algebraMatrix& operator*=(const double value);

	virtual algebraMatrix& operator/=(const double value);

	virtual algebraMatrix operator+(const algebraMatrix  &a);

	virtual algebraMatrix operator-(const algebraMatrix  &a);

	virtual algebraMatrix operator*(const algebraMatrix  &b);

	virtual algebraMatrix operator*(const double value) const;

	virtual algebraVector operator*(const algebraVector &vector);

	virtual algebraMatrix operator/(const double value) const;

	virtual bool operator==(const algebraMatrix& rhs) const;

	virtual bool operator!=(const algebraMatrix& rhs) const;

	virtual std::vector<std::vector <double> > getData() const {
		return _data;
	}

	virtual uint Rows() const;

	virtual uint Cols() const;

	virtual algebra::algebraVector Row(uint Row) const;

	virtual algebra::algebraVector Col(uint Col) const;

	virtual void Resize (uint nrows, uint ncols);

	virtual void Zero();

	virtual void ZeroRow(uint row);

	virtual void ZeroCol(uint row);

	virtual void UpdateCol(uint col, algebra::algebraVector v);

	virtual void UpdateRow(uint row, algebra::algebraVector v);

	virtual algebraMatrix Transpose() const;

	double SumRow(unsigned int Row) const;

	virtual bool CheckSymmetry();

	friend std::ostream& operator<<(std::ostream& Stream, const algebra::algebraMatrix& rhs);

private:
	std::vector<std::vector <double> > _data;

	uint _rows;

	uint _cols;
};

std::ostream& operator<<(std::ostream& Stream, const algebra::algebraMatrix& rhs);

} /* namespace algebra */

#endif /* SRC_ALGEBRA_ALGEBRAMATRIX_H_ */
