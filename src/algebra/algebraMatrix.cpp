#include "algebraMatrix.h"

#include <stdexcept>
#include <cmath>

namespace algebra
{
	algebraMatrix::algebraMatrix(uint rows, uint cols):
		_rows(rows), _cols(cols)
	{
		_data.resize(_rows, std::vector <double>(_cols));
	}

	algebraMatrix::algebraMatrix(const algebraMatrix &M)
	{
		_rows = M.Rows();
		_cols = M.Cols();

		_data = M.getData();
	}

	double& algebraMatrix::operator()(uint row, uint col)
	{
		if ( (row >= 0) && (col >= 0) && (row < _rows) && (col < _cols))
			return _data[row][col];
		else
			throw std::logic_error("algebraMatrix: Out Of Size");
	}

	const double& algebraMatrix::operator()(uint row, uint col) const
	{
		if ((row >= 0) && (col >= 0) && (row < _rows) && (col < _cols))
			return _data[row][col];
		else
			throw std::logic_error("algebraMatrix: Out Of Size");
	}

	algebraMatrix& algebraMatrix::operator= (const algebraMatrix &M)
	{
		if ((M.Rows() == _rows) && (M.Cols() == _cols))
			_data = M.getData();

		return *this;
	}

	algebraMatrix& algebraMatrix::operator+=(const algebraMatrix &M)
	{
		if ((M.Rows() == _rows) && (M.Cols() == _cols))
			for (uint i = 0; i < _rows; i++)
				for (uint j = 0; j < _cols; j++)
					(*this)(i, j) += M(i, j);
		else
			throw std::logic_error("Size Mismatch in algebraMatrix& algebraMatrix::operator+= (const algebraMatrix &algebraMatrix)");

		return *this;
	}

	algebraMatrix& algebraMatrix::operator-=(const algebraMatrix &M)
	{
		if ((M.Rows() == _rows) && (M.Cols() == _cols))
			for (uint i = 0; i < _rows; i++)
				for (uint j = 0; j < _cols; j++)
					(*this)(i, j) -= M(i, j);
		else
			throw std::logic_error("Size Mismatch in algebraMatrix& algebraMatrix::operator-= (const algebraMatrix &algebraMatrix)");

		return *this;
	}

	algebraMatrix& algebraMatrix::operator*=(const double value)
	{
		for (uint i = 0; i < _rows; i++)
			for (uint j = 0; j < _cols; j++)
				(*this)(i, j) *= value;

		return *this;
	}

	algebraMatrix& algebraMatrix::operator/=(const double value)
	{
		if (value)
			for (uint i = 0; i < _rows; i++)
				for (uint j = 0; j < _cols; j++)
					(*this)(i, j) /= value;
		else
			throw std::logic_error("Zero division in algebraMatrix& algebraMatrix::operator/= (const double value)");

		return *this;
	}

	uint algebraMatrix::Rows() const
	{
		return _rows;
	}

	uint algebraMatrix::Cols() const
	{
		return _cols;
	}

	void algebraMatrix::Resize(uint nrows, uint ncols)
	{
		_rows = nrows;
		_cols = ncols;

		_data.resize(_rows);

		for (std::vector<std::vector <double> >::iterator row = _data.begin(); row != _data.end(); row++)
			row->resize(_cols);
	}

	algebraVector algebraMatrix::Row(uint row) const
	{
		if ( row < _rows && row >= 0)
			return algebraVector(_data[row]);
		else
			throw std::logic_error("Row is out of range in algebraVector algebraMatrix::Row(int Row)");

		return 0;
	}

	algebraVector algebraMatrix::Col(uint col) const
	{
		if (col < _cols && col >= 0)
		{
			algebraVector result(_rows);
			for (uint i = 0; i < _rows; i++)
				result(i) = (*this)(i, col);
			return result;
		}
		else
			throw std::logic_error("Col is out of range in algebraVector algebraMatrix::Col(int Col)");

		return 0;
	}

	void algebraMatrix::Zero()
	{
		for (uint i = 0; i < _rows; i++)
			for (uint j = 0; j < _cols; j++)
				(*this)(i, j) = 0.0;
	}

	algebraMatrix algebraMatrix::operator+(const algebraMatrix &a)
	{
		algebraMatrix result(*this);
		result += a;
		return result;
	}

	algebraMatrix algebraMatrix::operator-(const algebraMatrix &a)
	{
		algebraMatrix result(*this);
		result -= a;
		return result;
	}

	algebraMatrix algebraMatrix::operator*(const algebraMatrix &b)
	{
		if (_cols == b.Rows())
		{
			uint bcols = b.Cols();
			algebraMatrix result(_rows, bcols);

			for (uint i = 0; i < _rows; i++)
			{
				algebraVector arow = Row(i);
				for (uint j = 0; j < bcols; j++)
				{
					double sum = 0.0;
					algebraVector bcol = b.Col(j);
					for (uint k = 0; k < _cols; k++)
						sum += arow(k) * bcol(k);

					result(i, j) = sum;
				}
			}
			return result;
		}
		else
			throw std::logic_error("algebraMatrix algebraMatrix::operator*(const algebraMatrix &b): Sizes Mismatch");
	}

	algebraVector algebraMatrix::operator*(const algebraVector &v)
	{
		if (v.Size() == _cols)
		{
			algebraVector result(_rows);
			for (uint i = 0; i < _rows; i++)
			{
				double sum = 0;
				for (uint j = 0; j < _cols; j++)
					sum += v(j) * (*this)(i, j);

				result(i) = sum;
			}
			return result;
		}
		else
			throw std::logic_error("algebraVector algebraMatrix::operator* (const algebraVector &vector): Sizes Mismatch");
	}

	algebraMatrix algebraMatrix::operator*(const double value) const
	{
		algebraMatrix result(*this);
		result *= value;
		return result;
	}

	algebraMatrix algebraMatrix::operator/(const double value) const
	{
		algebraMatrix result(*this);
		result /= value;
		return result;
	}

	void algebraMatrix::ZeroRow(uint row)
	{
		if (row < _rows)
			for (uint i = 0; i < _cols; i++)
				(*this)(row, i) = 0;
	}

	void algebraMatrix::ZeroCol(uint col)
	{
		if (col < _cols)
			for (uint i = 0; i < _rows; i++)
				(*this)(i, col) = 0;
	}

	void algebraMatrix::UpdateCol(uint col, algebraVector v)
	{
		if ( col >= 0 && col < _cols && _rows == v.Size())
		{
			for (uint i = 0; i < _rows; i++)
				(*this)(i, col) = v(i);
		}
		else
			throw std::logic_error("void algebraMatrix::UpdateCol(uint col, algebraVector v): Sizes Mismatch");
	}

	void algebraMatrix::UpdateRow(uint row, algebraVector v)
	{
		if ( row >= 0 && row < _rows && _cols == v.Size())
		{
			for (uint i = 0; i < _cols; i++)
				(*this)(row, i) = v(i);
		}
		else
			throw std::logic_error("void algebraMatrix::UpdateRow(uint row, algebraVector v): Sizes Mismatch");
	}

	algebraMatrix algebraMatrix::Transpose() const
	{
		algebraMatrix result(_cols, _rows);

		for (uint i = 0; i < _rows; i++)
			for (uint j = 0; j < _cols; j++)
				result(j, i) = (*this)(i, j);

		return result;
	}

	bool algebraMatrix::operator==(const algebraMatrix& rhs) const
	{
		bool flag = true;
		if (Rows() == rhs.Rows() && Cols() == rhs.Cols())
			for (uint i = 0; i < Rows(); i++)
				for (uint j = 0; j < Cols(); j++)
					if ((*this)(i, j) != rhs(i, j))
					{
						flag = false;
						break;
					}
		else
			flag = false;

		return flag;
	}

	bool algebraMatrix::operator!=(const algebraMatrix& rhs) const
	{
		return !operator==(rhs);
	}

	double algebraMatrix::SumRow(unsigned int Row) const
	{
		double result = 0.0;
		if ((Row < _rows) && (Row >= 0))
		{
			for (unsigned int i = 0; i < _cols; i++)
				result += (*this)(Row, i);

			return result;
		}
		else
			throw std::logic_error("Row is out of range in AlgebraVector AlgebraMatrix::SumRow(int Row)");

		return result;
	}

	bool algebraMatrix::CheckSymmetry()
	{
		if ( _cols == _rows )
		{
			for (uint i = 0; i < _rows; i++)
				for (uint j = 0; j < i; j++)
					if ( std::fabs((*this)(i, j) - (*this)(j, i)) > 1e-12 )
					{
						std::cout << i << " " << j << " " << (*this)(i, j) << " " << (*this)(j, i) << std::endl;
						return false;
					}
		}
		else
			throw std::logic_error("bool algebraMatrix::CheckSymmetry(): Matrix not quad");

		return true;
	}

	std::ostream& operator<<(std::ostream& Stream, const algebraMatrix& rhs)
	{
		Stream.flags(std::ios::scientific | std::ios::left | std::ios::dec);
		Stream.precision(8);
		Stream.width(8);

		Stream << "______________________________________________________________________" << std::endl << std::endl;

		for (uint i = 0; i < rhs.Rows(); i++)
		{
			for (uint j = 0; j < rhs.Cols(); j++)
				if ( fabs(rhs(i, j)) < 1e-10 )
					Stream << 0.0 << " ";
				else
					Stream << rhs(i, j) << " ";

			Stream << std::endl;
		}

		Stream << "______________________________________________________________________\n\n";

		return Stream;
	}
} /* namespace algebra */
