#include "Clock.h"
#include <cmath>

using namespace std;

namespace utilities
{
namespace system
{

Clock_t Clock_t::operator-(const Clock_t &rhs) const
{
	timeval x = Time;
	timeval y = rhs.Time;

	if (fabs(x.tv_sec < y.tv_sec) ||  ( fabs(x.tv_sec == y.tv_sec) && fabs(y.tv_usec - x.tv_usec) <= 1e-6))
		return Clock_t();

	if (x.tv_usec < y.tv_usec)
	{
		unsigned long nsec = (y.tv_usec - x.tv_usec) / 1000000 + 1;
		y.tv_usec -= 1000000 * nsec;
		y.tv_sec += nsec;
	}

	x.tv_sec -= y.tv_sec;
	x.tv_usec -= y.tv_usec;

	return Clock_t(x);
}


ostream& operator<<(ostream& o, const Clock_t& rhs)
{
	Clock_t TV = rhs;
	short Seconds = TV.sec() % 60;
	short Minutes = (TV.sec() / 60) % 60;
	short Hours = (TV.sec() / 3600) % 24;
	short Days = Hours / 24;
	short Order = (TV.usec() == 0) ? 0 : (short)log10((long double)TV.usec()) + 1;

	o << "Time: ";

	if (Days == 1)
		o << Days << " Day ";
	else if (Days > 1)
		o << Days << " Days ";
	if (Hours < 10)
		o << "0";
	o << Hours << ":";
	if (Minutes < 10)
		o << "0";
	o << Minutes << ":";
	if (Seconds < 10)
		o << "0";
	o << Seconds << ".";
	if 		(Order == 0) o << "00000";
	else if	(Order == 1) o << "00000";
	else if (Order == 2) o << "0000";
	else if (Order == 3) o << "000";
	else if (Order == 4) o << "00";
	else if (Order == 5) o << "0";

	o << TV.usec();

	return o;
}

} /* namespace system */
} /* namespace utilities */
