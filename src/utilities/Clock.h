#ifndef SRC_UTILITIES_CLOCK_H_
#define SRC_UTILITIES_CLOCK_H_

#include <sys/time.h>
#include <iostream>

namespace utilities
{
namespace system
{

class Clock_t
{
public:
	Clock_t() {
		Time.tv_sec = 0;
		Time.tv_usec = 0;
	}

	Clock_t(timeval aTime) {
		Time = aTime;
	}

	~Clock_t() {}

	long sec () const {
		return Time.tv_sec;
	}

	long usec () const {
		return Time.tv_usec;
	}

	Clock_t operator-(const Clock_t &rhs) const;

	friend std::ostream& operator<<(std::ostream& o, const Clock_t& rhs);
private:
	timeval Time;
};

std::ostream& operator<<(std::ostream& o, const Clock_t& rhs);

} /* namespace system */
} /* namespace utilities */

#endif /* SRC_UTILITIES_CLOCK_H_ */
