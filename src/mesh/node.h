#ifndef SRC_MESH_NODE_H_
#define SRC_MESH_NODE_H_

#include <iostream>

#include "meshObject.h"

namespace mesh {

class node: public virtual meshObject {
public:
	node(double x, double y, double z): _x(x), _y(y), _z(z), _data(0), _dx(0), _dy(0), _dz(0) {
		createIndex(_x, _y, _z);
	}

	node(double x, double y, double z, double data): _x(x), _y(y), _z(z), _data(data),  _dx(0), _dy(0), _dz(0) {
		createIndex(_x, _y, _z);
	}

	virtual ~node() {}

	virtual double x() const { return _x; }

	virtual double y() const { return _y; }

	virtual double z() const { return _z;}

	double dx() const { return _dx; }

	double dy() const { return _dy; }

	double dz() const { return _dz;}

	void dxdydz(int i, double d);

	void dxdydz(double dx, double dy, double dz);

	double di(int i) const;

	void xyz(double x, double y, double z);


	double data() const { return _data; }
	void data(double data) { _data = data; }

protected:
	friend std::ostream& operator<<(std::ostream& stream, const node& rhs);

private:
	double _x;
	double _y;
	double _z;

	double _data;

	double _dx;
	double _dy;
	double _dz;
};

std::ostream& operator<<(std::ostream& stream, const node& rhs);

} /* namespace mesh */

#endif /* SRC_MESH_NODE_H_ */
