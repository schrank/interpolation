#ifndef SRC_MESH_PRIMITIVEELEMENT_H_
#define SRC_MESH_PRIMITIVEELEMENT_H_

#include <iostream>
#include <string>
#include <vector>

#include "meshObject.h"
#include "node.h"

namespace mesh {

class primitiveElement: public virtual meshObject
{
public:
	primitiveElement() {
	}

	virtual ~primitiveElement() {
		nodes.clear();
	}

	unsigned int numberOfNodes() const {
		return nodes.size();
	}

	std::vector<node*> getNodes() const {
		return nodes;
	}

	node* getNode(unsigned int aNode) {
		return *(std::next( nodes.begin(), aNode ));
	}

	node centreOfMass() const {
		double x = 0.0, y = 0.0, z = 0.0;

		for (node* n: nodes) { x += n->x(); y += n->y(); z += n->z();}

		return node(x / nodes.size(), y / nodes.size(), z / nodes.size());
	}

	//Двухмерные типы:
	//1		-	VTK_VERTEX
	//3		-	VTK_LINE
	//5		-	VTK_TRIANGLE
	//7		-	VTK_POLYGON(n-угольник)
	//8		-	VTK_PIXEL(квадрат)
	//9		-	VTK_QUAD(четырехугольник)
	//Трехмерные типы:
	//10	-	VTK_TETRA
	//11	-	VTK_VOXEL
	//12	-	VTK_HEXAHEDRON
	//13	-	VTK_WEDGE
	//14	-	VTK_PYRAMID
	virtual int type() const = 0;

protected:
	friend std::ostream& operator<<(std::ostream& stream, const primitiveElement& rhs);

protected:
	std::vector<node*> nodes;
};

std::ostream& operator<<(std::ostream& stream, const primitiveElement& rhs);

} /* namespace mesh */

#endif /* SRC_MESH_PRIMITIVEELEMENT_H_ */
