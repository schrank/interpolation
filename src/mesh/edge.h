#ifndef SRC_MESH_EDGE_H_
#define SRC_MESH_EDGE_H_

#include "node.h"
#include "primitiveElement.h"

namespace mesh {

class edge: public virtual primitiveElement {
public:
	edge(node* a, node* b);

	virtual ~edge();

	// 3	-	VTK_LINE
	int type() const {
		return 3;
	}
};

} /* namespace mesh */

#endif /* SRC_MESH_EDGE_H_ */
