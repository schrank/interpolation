#ifndef SRC_MESH_MESH_H_
#define SRC_MESH_MESH_H_

#include <set>
#include <string>

#include "node.h"
#include "edge.h"

namespace mesh {

class Mesh {
public:
	Mesh() {}

	virtual ~Mesh();

	virtual node* addNode(double x, double y, double z);

	node* addNode(double x, double y, double z, double data);

	edge* addEdge(node* a, node* b);

	primitiveElement* addTriangle(node* a, node* b, node* c);

	virtual primitiveElement* addQuad(node* a, node* b, node* c, node* d);

	primitiveElement* addHexahedron(node* a, node* b, node* c, node* d, node* e, node* f, node* g, node* h);

	std::set<node*> getNodes() const {
		return std::set<node*>(nodes.begin(), nodes.end());
	}

	std::set<primitiveElement*> getElements() const {
		return elements;
	}

	unsigned int numNodes() const {
		return nodes.size();
	}

	node* getNode(unsigned int aNode) {
		return *(std::next( nodes.begin(), aNode ));
	}

	unsigned int findNode(node* aNode) const {
		return std::distance(nodes.begin(), nodes.find(aNode));
	}

	virtual void importMSH(std::string file = "mesh");

	void importKIM(std::string file = "mesh");

	void importGMSH(std::string file = "mesh");

	void importVTU(std::string file = "mesh");

	void export2vtp(std::string file = "mesh");

	virtual void export2vtu(std::string file = "mesh");

protected:
	std::set<primitiveElement*> elements;

	std::set<node*> nodes;

	std::set<edge*> edges;
};

template <class T> std::string to_string(const T& t);

void dataArrayNode(std::ofstream& o, std::string params, const char* type, const char* name,  int noc = 0);

} /* namespace mesh */

#endif /* SRC_MESH_MESH_H_ */
