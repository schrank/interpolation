#include "primitiveElement.h"

#include <sstream>

namespace mesh
{

std::ostream& operator<<(std::ostream& stream, const primitiveElement& rhs)
{
	stream.flags(std::ios::scientific | std::ios::left | std::ios::dec);
	stream.precision(6);

	for (node* n : rhs.getNodes())
		stream << "( " << n->x() << " " << n->y() << " " << n->z() << " )\n";

	return stream;
}

} /* namespace mesh */
