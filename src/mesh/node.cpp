#include "node.h"

#include "sstream"
#include <stdexcept>

namespace mesh {

void node::dxdydz(int i, double di)
{
	if ( i == 0 )
		_dx = di;
	else if ( i == 1 )
		_dy = di;
	else if ( i == 2 )
		_dz = di;
	else
		throw std::logic_error("void node::dxdydz: Index i is out of dimension!");
}

double node::di(int i) const
{
	if ( i == 0 )
		return _dx;
	else if ( i == 1 )
		return _dy;
	else
		return _dz;

	throw std::logic_error("void node::dxdydz: Index i is out of dimension!");
}

void node::xyz(double x, double y, double z)
{
	_x = x, _y = y, _z = z;
}

void node::dxdydz(double dx, double dy, double dz)
{
	_dx = dx, _dy = dy, _dz = dz;
}


std::ostream& operator<<(std::ostream& stream, const node& rhs)
{
	stream.flags(std::ios::scientific | std::ios::left | std::ios::dec);
	stream.precision(6);

	stream << "( " << rhs.x() << " " << rhs.y() << " " << rhs.z() << " )" << "\n";

	return stream;
}

} /* namespace mesh */
