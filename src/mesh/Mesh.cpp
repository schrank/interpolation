#include "Mesh.h"

#include <stdexcept>
#include <fstream>
#include <sstream>
#include <vector>

#include "triangle.h"
#include "quad.h"
#include "hexahedron.h"

using namespace std;

namespace mesh {

Mesh::~Mesh()
{
	elements.clear();
	nodes.clear();
}

node* Mesh::addNode(double x, double y, double z)
{
	node *aNode = new node(x, y, z);

	if ( nodes.insert(aNode).second )
		return aNode;

	std::cout << "Node: " << *aNode << "is exist!\n";

	return *nodes.find(aNode);
}

node* Mesh::addNode(double x, double y, double z, double data)
{
	node *aNode = new node(x, y, z, data);

	if ( nodes.insert(aNode).second )
		return aNode;

	std::cout << "Node: " << *aNode << "is exist!\n";

	return *nodes.find(aNode);
}

edge* Mesh::addEdge(node* a, node* b)
{
	edge *aEdge = new edge(a, b);

	if ( edges.insert(aEdge).second )
		return aEdge;

	std::cout << "Edge: " << *aEdge << "is exist!\n";

	return *edges.find(aEdge);
}

primitiveElement* Mesh::addTriangle(node* a, node* b, node* c)
{
	primitiveElement *aTriangle = new triangle(a, b, c);

	if ( elements.insert(aTriangle).second )
		return aTriangle;

	std::cout << "Triangle: " << *aTriangle << "is exist!\n";

	return *elements.find(aTriangle);
}

primitiveElement* Mesh::addQuad(node* a, node* b, node* c, node* d)
{
	primitiveElement *aQuad = new quad(a, b, c, d);

	if ( elements.insert(aQuad).second )
		return aQuad;

	std::cout << "Quad: " << *aQuad << "is exist!\n";

	return *elements.find(aQuad);
}

primitiveElement* Mesh::addHexahedron(node* a, node* b, node* c, node* d, node* e, node* f, node* g, node* h)
{
	primitiveElement *aHexahedron = new hexahedron(a, b, c, d, e, f, g, h);

	if ( elements.insert(aHexahedron).second )
		return aHexahedron;

	std::cout << "Hexahedron: " << *aHexahedron << "is exist!\n";

	return *elements.find(aHexahedron);
}


void Mesh::importMSH(std::string file)
{
	//файл с входной сеткой
	std::ifstream infile(file.c_str());
	if ( infile.fail() )
		throw std::logic_error("Error - no input mesh file!");

	int numNodes; // число узлов
	int numElements; // число элементов
	infile >> numNodes >> numElements;

	std::vector<node*> MasNodes(numNodes);

	//reading coordinates of nodes and create node
	int i;
	double x, y;
	for (i = 0; i < numNodes; i++)
	{
		infile >> x >> y;

		MasNodes[i] = addNode(x, y, 0);

		if ( !(i % 1000) && i)
			std::cout << "added " << i << " nodes" << std::endl;
	}
	std::cout << "added " << i << " nodes" << std::endl;

	for (i = 0; i < numElements; i++)
	{
		std::vector<int> quadNodes(4);

		for (int j = 0; j < 4; j++)
		{
			int idx;
			infile >> idx;

			quadNodes[j] = findNode(MasNodes[--idx]);
		}

		addQuad(getNode(quadNodes[0]), getNode(quadNodes[1]), getNode(quadNodes[2]), getNode(quadNodes[3]));

		if ( !(i % 1000) && i)
			std::cout << "added " << i << " elements" << std::endl;
	}

	std::cout << "added " << i << " elements" << std::endl;

	infile.close();
}

void Mesh::importGMSH(std::string file)
{
	//файл с входной сеткой
	ifstream infile(file.c_str());
	if (!infile)
		cout<<"Error - no input file";

	string line;
	int dim, numNodes, i;
	infile >> line;
	infile >> line;
	infile >> line;
	infile >> dim;
	infile >> line;
	infile >> numNodes;

	//reading coordinates of nodes and create node
	int numObject;
	double x, y, z;
	std::vector<node*> MasNodes(numNodes);
	for (i = 0; i < numNodes; i++)
	{
		infile >> x >> y >> z >> numObject;

		MasNodes[i] = addNode(x, y, z);

		if (!(i%1000) && (i != 0))
			cout << "added " << i << " nodes" << endl;
	}
	cout << "added " << i << " nodes" << endl;

	infile >> line;
	int numEdges; // число ребер
	infile >> numEdges;
	for (i = 0; i < numEdges; i++)
		infile >> x >> y >> numObject;

	std::string typeOfElement;
	int num, numElements; // число элементов
	// Считывание элементов. В зависимости от типа элемента вызывается нужная процедура создания элемента
	infile >> typeOfElement;

	while (typeOfElement.compare("End") != 0)
	{
		infile >> numElements;
		// создаются треугольники
		if (typeOfElement.compare("Triangles") == 0)
		{
			for (i = 0; i < numElements; i++)
			{
				std::vector<int> triangleNodes(3);
				for (int j = 0; j < 3; j++)
				{
					infile >> num;
					triangleNodes[j] = findNode(MasNodes[--num]);
				}

				addTriangle(getNode(triangleNodes[0]),
						getNode(triangleNodes[1]), getNode(triangleNodes[2]));

				infile >> numObject;

				if (!(i % 1000) && (i != 0))
					cout << "added " << i << " " << typeOfElement << endl;
			}
		}
		else if (typeOfElement.compare("Quadrilaterals") == 0)
		{
			for (i = 0; i < numElements; i++)
			{
				std::vector<int> quadNodes(4);
				for (int j = 0; j < 4; j++)
				{
					infile >> num;
//					num--; /// lame!!!!
					quadNodes[j] = findNode(MasNodes[--num]);
				}

				addQuad(getNode(quadNodes[0]), getNode(quadNodes[1]), getNode(quadNodes[2]), getNode(quadNodes[3]));

				infile >> numObject;

				if (!(i % 1000) && (i != 0))
					cout << "added " << i << " " << typeOfElement << endl;
			}
		}
		cout << "added " << i << " " << typeOfElement << endl;
		infile >> typeOfElement;
	}

	infile.close();
}

void Mesh::importVTU(std::string filename)
{
	filename += ".vtu";

	// файл с входной сеткой
	std::ifstream infile(filename.c_str());

	if ( infile.fail() )
		throw std::logic_error("Error - no input mesh file!");

	std::string line;

	std::getline(infile, line);
	std::getline(infile, line);
	std::getline(infile, line);
	std::getline(infile, line);

	size_t first = line.find("\"");
	size_t second = line.find("\"", first + 1);
	// число узлов
	unsigned int numNodes = std::stoi( std::string(line.begin() + first + 1, line.begin() + second) );

	first = line.find("\"", second + 1);
	second = line.find("\"", first + 1);

	// число элементов
	unsigned int numElements = std::stoi( std::string(line.begin() + first + 1, line.begin() + second) );

	do
	{
		std::getline(infile, line);
	}
	while ( line.find("coordinates") == std::string::npos );

	std::vector<node*> MasNodes(numNodes);

	double x, y, z;
	unsigned int i;
	for ( i = 0; i < numNodes; i++ )
	{
		infile >> x >> y >> z;

		MasNodes[i] = addNode(x, y, z);

		if ( !(i % 1000) && i)
			std::cout << "added " << i << " nodes" << std::endl;
	}
	std::cout << "added " << i << " nodes" << std::endl;

	std::string connectivity, offsets, types;
	do
	{
		std::getline(infile, connectivity);
	}
	while ( connectivity.find("connectivity") == std::string::npos );

	vector<double> conn;

//	std::getline(infile, connectivity);
	for (i = 0; i < numElements * 8; i++)
	{
		double tmp;
		infile >> tmp;
		conn.push_back(tmp);
	}

	do
	{
		std::getline(infile, offsets);
	}
	while ( offsets.find("offsets") == std::string::npos );

	vector<int> off;
	for (i = 0; i < numElements; i++)
	{
		int tmp;
		infile >> tmp;
		off.push_back(tmp);
	}

//	std::getline(infile, offsets);

	do
	{
		std::getline(infile, types);
	}
	while ( types.find("types") == std::string::npos );

//	std::getline(infile, types);
	vector<int> typ;
	for (i = 0; i < numElements; i++)
	{
		int tmp;
		infile >> tmp;
		typ.push_back(tmp);
	}

	std::string::size_type sz_types = 0;
	std::string::size_type sz_offsets = 0;
	std::string::size_type sz_con = 0;
	std::string::size_type tmp_t = 0;
	std::string::size_type tmp_o = 0;
	std::string::size_type tmp_c = 0;
	int offset_old = 0, offset = 0;

	for (i = 0; i < numElements; i++)
	{
//		int type = std::stoi(types.substr(tmp_t), &sz_types);
//		tmp_t += sz_types;
//		offset_old = offset;
//		offset = std::stoi(offsets.substr(tmp_o), &sz_offsets);
//
//		int dof = offset - offset_old;
//
//		tmp_o += sz_offsets;
//
//		std::vector<int> nodes(dof);
//
//		int idx;
//		for (int j = 0; j < dof; j++)
//		{
//			idx = std::stoi(connectivity.substr(tmp_c), &sz_con);
//			tmp_c += sz_con;
//
//			nodes[j] = findNode(MasNodes[idx]);
//		}

		int type = typ[i];
		offset_old = offset;
		offset = off[i];

		int dof = offset - offset_old;

		std::vector<int> nodes(dof);

		int idx;
		for (int j = 0; j < dof; j++)
		{
			idx = conn[i * dof + j];

			nodes[j] = findNode(MasNodes[idx]);
		}

		switch (type)
		{
		case 5:
			addTriangle(getNode(nodes[0]), getNode(nodes[1]), getNode(nodes[2]));
			break;
		case 9:
			addQuad(getNode(nodes[0]), getNode(nodes[1]), getNode(nodes[2]), getNode(nodes[3]));
			break;
		case 12:
			addHexahedron(getNode(nodes[0]), getNode(nodes[1]), getNode(nodes[2]), getNode(nodes[3]),
					getNode(nodes[4]), getNode(nodes[5]), getNode(nodes[6]), getNode(nodes[7]));
			break;
		default:
			break;
		}

		if ( !(i % 1000) && i)
			std::cout << "added " << i << " elements" << std::endl;
	}

	std::cout << "added " << i << " elements" << std::endl;
}

void Mesh::importKIM(std::string file)
{
	//файл с входной сеткой
	std::ifstream infile(file.c_str());
	if ( infile.fail() )
		throw std::logic_error("Error - no input mesh file!");

	int numNodes; // число узлов
	int numElements; // число элементов
	infile >> numNodes >> numElements;

	std::vector<node*> MasNodes(numNodes);

	//reading coordinates of nodes and create node
	int i;
	double x, y, z, dx, dy, dz;

	for (i = 0; i < numNodes; i++)
	{
		infile >> x >> y >> z >> dx >> dy >> dz;

		MasNodes[i] = addNode(x, y, z);

		MasNodes[i]->dxdydz(dx, dy, dz);

		if ( !(i % 1000) && i)
			std::cout << "added " << i << " nodes" << std::endl;
	}
	std::cout << "added " << i << " nodes" << std::endl;

	for (i = 0; i < numElements; i++)
	{
		std::vector<int> hexNodes(8);

		for (int j = 0; j < 8; j++)
		{
			int idx;
			infile >> idx;

			hexNodes[j] = findNode(MasNodes[idx]);
		}

		addHexahedron( getNode(hexNodes[0]), getNode(hexNodes[1]), getNode(hexNodes[2]), getNode(hexNodes[3]),
				getNode(hexNodes[4]), getNode(hexNodes[5]), getNode(hexNodes[6]), getNode(hexNodes[7]));

		if ( !(i % 1000) && i)
			std::cout << "added " << i << " elements" << std::endl;
	}

	std::cout << "added " << i << " elements" << std::endl;

	infile.close();
}


void Mesh::export2vtp(std::string file)
{
	file += ".vtp";

	std::ofstream out(file.c_str());

	if ( out.fail() ) throw std::logic_error("Mesh::export2vtp: Error in creation of mesh file!");

	out << "<?xml version=\"1.0\"?>\n";
	out << "<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
	out << "  <PolyData>\n";
	out << "    <Piece"
					" NumberOfPoints=\""<< nodes.size() <<
					"\" NumberOfPolys=\"" << elements.size() <<
					"\" NumberOfLines=\"" << edges.size() <<
					"\" NumberOfVerts=\"" << nodes.size() <<
					"\">\n";

	std::string coordinates;
	std::string data;
	for (node* aNode : nodes)
	{
		coordinates += to_string(aNode->x()) + " "
						+ to_string(aNode->y()) + " " + to_string(aNode->z()) + " ";

		data += to_string(aNode->data()) + " ";
	}

	out << "      <PointData Scalars=\"scalars\">\n";
	dataArrayNode(out, data, "Float64", "data", 1);
	out << "      </PointData>\n";

	out << "      <Points>\n";
	dataArrayNode(out, coordinates, "Float64", "coordinates", 3);
	out << "      </Points>\n";

	// connectivity - список связности элементов
	std::string connectivity;
	// offsets - список индексов начальных координат каждого элемента в списке connectivity
	//
	// например, если все элементы являются четырехугольниками, а список connectivity имеет след. вид:
	// 0 1 2 3 4 5 6 7 0 1 5 4 2 3 7 6 0 4 7 3 1 2 6 5
	// Список offsets будет выглядеть так: 4 8 12 16 20 24
	std::string offsets;
	int offset = 0;

	//types - список типов элементов сетки
	//Двухмерные типы:
	//1		-	VTK_VERTEX
	//3		-	VTK_LINE
	//5		-	VTK_TRIANGLE
	//7		-	VTK_POLYGON(n-угольник)
	//8		-	VTK_PIXEL(квадрат)
	//9		-	VTK_QUAD(четырехугольник)
	//Трехмерные типы:
	//10	-	VTK_TETRA
	//11	-	VTK_VOXEL
	//12	-	VTK_HEXAHEDRON
	//13	-	VTK_WEDGE
	//14	-	VTK_PYRAMID
	std::string types;

	for (primitiveElement* element : elements)
	{
		int numberOfNodes = element->numberOfNodes();

		offset += numberOfNodes;
		offsets += to_string(offset) + " ";

		types += to_string(element->type()) + " ";

		std::vector<node*> nodes = element->getNodes();
		for (node* n : nodes)
		{
			connectivity += to_string(findNode(n)) + " ";
		}
	}

	out << "      <Polys>\n";
	dataArrayNode(out, connectivity, "Int64", "connectivity");
	dataArrayNode(out, offsets, "Int64", "offsets");
	dataArrayNode(out, types, "Int64", "types");
	out << "      </Polys>\n";

	offsets = "";
	offset = 0;
	connectivity = "";
	for (primitiveElement* edge : edges)
	{
		int numberOfNodes = edge->numberOfNodes();

		offset += numberOfNodes;
		offsets += to_string(offset) + " ";

		std::vector<node*> nodes = edge->getNodes();
		for (node* n : nodes)
			connectivity += to_string(findNode(n)) + " ";
	}

	out << "      <Lines>\n";
	dataArrayNode(out, connectivity, "Int64", "connectivity");
	dataArrayNode(out, offsets, "Int64", "offsets");
	out << "      </Lines>\n";

	offsets = "";
	connectivity = "";
	for (unsigned int index = 0; index < nodes.size(); index++)
	{
		connectivity += to_string(index) + " ";
		offsets += to_string(index + 1) + " ";
	}

	out << "      <Verts>\n";
	dataArrayNode(out, connectivity, "Int64", "connectivity");
	dataArrayNode(out, offsets, "Int64", "offsets");
	out << "      </Verts>\n";


	out << "    </Piece>\n";
	out << "</PolyData>\n";
	out << "</VTKFile>";

	out.close();
}

void Mesh::export2vtu(std::string file)
{
	const unsigned int numOfNodes = nodes.size();
	const unsigned int numOfElements = elements.size();

	file += ".vtu";

	std::ofstream out(file.c_str());
	if ( out.fail() ) throw std::logic_error ("Mesh::export2vtu: Error with mesh file");

	out << "<?xml version=\"1.0\"?>\n";
	out << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
	out << "  <UnstructuredGrid>\n";
	out << "    <Piece NumberOfPoints=\""<< numOfNodes << "\" NumberOfCells=\"" << numOfElements << "\">\n";

	// координаты точек
	std::string coordinates;
	std::string data;
	std::string displacement;
	for (node* aNode : nodes)
	{
		coordinates += to_string(aNode->x()) + " "
						+ to_string(aNode->y()) + " " + to_string(aNode->z()) + " ";

		data += to_string(aNode->data()) + " ";

		displacement +=
				  to_string(aNode->dx()) + " "
				+ to_string(aNode->dy()) + " "
				+ to_string(aNode->dz()) + " ";
	}

	out << "      <PointData Scalars=\"scalars\">\n";
	dataArrayNode(out, data, "Float64", "data", 1);
	dataArrayNode(out, displacement, "Float64", "displacement", 3);
	out << "      </PointData>\n";

	out << "      <Points>\n";
	dataArrayNode(out, coordinates, "Float64", "coordinates", 3);
	out << "      </Points>\n";

	// connectivity - список связности элементов
	std::string connectivity;
	// offsets - список индексов начальных координат каждого элемента в списке connectivity
	//
	// например, если все элементы являются четырехугольниками, а список connectivity имеет след. вид:
	// 0 1 2 3 4 5 6 7 0 1 5 4 2 3 7 6 0 4 7 3 1 2 6 5
	// Список offsets будет выглядеть так: 4 8 12 16 20 24
	std::string offsets;
	int offset = 0;

	//types - список типов элементов сетки
	//Двухмерные типы:
	//1		-	VTK_VERTEX
	//3		-	VTK_LINE
	//5		-	VTK_TRIANGLE
	//7		-	VTK_POLYGON(n-угольник)
	//8		-	VTK_PIXEL(квадрат)
	//9		-	VTK_QUAD(четырехугольник)
	//Трехмерные типы:
	//10	-	VTK_TETRA
	//11	-	VTK_VOXEL
	//12	-	VTK_HEXAHEDRON
	//13	-	VTK_WEDGE
	//14	-	VTK_PYRAMID
	std::string types;

	for (primitiveElement* element : elements)
	{
		int numberOfNodes = element->numberOfNodes();

		offset += numberOfNodes;
		offsets += to_string(offset) + " ";

		types += to_string(element->type()) + " ";

		std::vector<node*> nodes = element->getNodes();
		for (node* n : nodes)
		{
			connectivity += to_string(findNode(n)) + " ";
		}
	}

	out << "      <Cells>\n";
	dataArrayNode(out, connectivity, "Int64", "connectivity");
	dataArrayNode(out, offsets, "Int64", "offsets");
	dataArrayNode(out, types, "Int64", "types");
	out << "      </Cells>\n";

	out << "    </Piece>\n";
	out << "</UnstructuredGrid>\n";
	out << "</VTKFile>";

	out.close();
}

template <class T> std::string to_string(const T& t)
{
	std::stringstream ss;
	ss.precision(12);
	ss << t;
	return ss.str();
}

void dataArrayNode(std::ofstream& o, std::string params, const char* type, const char* name, int noc)
{
	if (!params.empty())
	{
		std::string h;
		char header[200];

		h = "        <DataArray type=\"%s\" Name=\"%s\" ";
		if (noc > 0)
		{
			h +=  "NumberOfComponents=\"%i\" ";
		}
		h += "Format=\"ascii\">\n";

		(noc > 0) ?
			sprintf(header, h.c_str(), type, name, noc) :
			sprintf(header, h.c_str(), type, name);

		o << header << params + "\n" << "        </DataArray>\n";
	}
}

} /* namespace mesh */
