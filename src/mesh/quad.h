#ifndef SRC_MESH_QUAD_H_
#define SRC_MESH_QUAD_H_

#include "primitiveElement.h"

namespace mesh {

class quad: public virtual primitiveElement {
public:
	quad(node* a, node* b, node* c, node* d);

	virtual ~quad();

	//9		-	VTK_QUAD(четырехугольник)
	int type() const {
		return 9;
	}
};

} /* namespace mesh */

#endif /* SRC_MESH_QUAD_H_ */
