#include "hexahedron.h"

namespace mesh
{

hexahedron::hexahedron(node* a, node* b, node* c, node* d, node* e, node* f, node* g, node* h)
{
	double x = (a->x() + b->x() + c->x() + d->x()
					+ e->x() + f->x() + g->x() + h->x()) / 8.0;
	double y = (a->y() + b->y() + c->y() + d->y()
					+ e->y() + f->y() + g->y() + h->y()) / 8.0;
	double z = (a->z() + b->z() + c->z() + d->z()
					+ e->z() + f->z() + g->z() + h->z()) / 8.0;

	createIndex(x, y, z);

	nodes.push_back(a);
	nodes.push_back(b);
	nodes.push_back(c);
	nodes.push_back(d);

	nodes.push_back(e);
	nodes.push_back(f);
	nodes.push_back(g);
	nodes.push_back(h);

}

hexahedron::~hexahedron() {}

} /* namespace mesh */
