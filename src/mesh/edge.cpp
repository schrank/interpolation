#include "edge.h"

namespace mesh {

edge::edge(node* a, node* b) {
	double x = (a->x() + b->x()) / 2.0;
	double y = (a->y() + b->y()) / 2.0;
	double z = (a->z() + b->z()) / 2.0;

	createIndex(x, y, z);

	nodes.push_back(a);
	nodes.push_back(b);
}

edge::~edge() {
}

} /* namespace mesh */
