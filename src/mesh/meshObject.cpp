#include "meshObject.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdio.h>

namespace mesh {

void meshObject::createIndex(const double x, const double y, const double z)
{
	_index = new index(x, y, z);
}

bool meshObject::operator<(const meshObject& ameshObject) const
{
	return false;
}

} /* namespace mesh */
