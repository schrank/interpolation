#include "triangle.h"

namespace mesh {

triangle::triangle(node* a, node* b, node* c)
{
	double x = (a->x() + b->x() + c->x()) / 3.0;
	double y = (a->y() + b->y() + c->y()) / 3.0;
	double z = (a->z() + b->z() + c->z()) / 3.0;

	createIndex(x, y, z);

	nodes.push_back(a);
	nodes.push_back(b);
	nodes.push_back(c);
}

triangle::~triangle() {
}

} /* namespace mesh */
