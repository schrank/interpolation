#ifndef SRC_MESH_TRIANGLE_H_
#define SRC_MESH_TRIANGLE_H_

#include <set>

#include "node.h"
#include "edge.h"
#include "primitiveElement.h"

namespace mesh {

class triangle: public virtual primitiveElement {
public:
	triangle(node* a, node* b, node* c);

	virtual ~triangle();

	//5		-	VTK_TRIANGLE
	int type() const {
		return 5;
	}
};

} /* namespace mesh */

#endif /* SRC_MESH_TRIANGLE_H_ */
