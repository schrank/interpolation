#ifndef SRC_MESH_INDEX_H_
#define SRC_MESH_INDEX_H_

#include <iostream>

namespace mesh
{

class index
{
public:
	index(double x, double y, double z): _x(x), _y(y), _z(z) {}

	bool operator<(const index& idx) const {
		if (_x  < idx.x())
			return true;

		if (_y  < idx.y())
			return true;

		if (_z  < idx.z())
			return true;

		return false;

	}

	void xyz(double x, double y, double z) {
		_x = x, _y = y, _z = z;
	}

	double x() const {
		return _x;
	}

	double y() const {
		return _y;
	}

	double z() const {
		return _z;
	}

	~index() {}

//protected:
//	friend std::ostream& operator<<(std::ostream& stream, const index& rhs);

private:
	double _x;
	double _y;
	double _z;
};

//std::ostream& operator<<(std::ostream& stream, const index& rhs)
//{
//	stream.flags(std::ios::scientific | std::ios::left | std::ios::dec);
//	stream.precision(6);
//
//	stream << rhs.x() << " " << rhs.y() << " " << rhs.z() << "\n";
//
//	return stream;
//}


} /* namespace mesh */

#endif /* SRC_MESH_INDEX_H_ */
