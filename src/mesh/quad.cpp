#include "quad.h"

namespace mesh {

quad::quad(node* a, node* b, node* c, node* d)
{
	double x = (a->x() + b->x() + c->x() + d->x()) / 4.0;
	double y = (a->y() + b->y() + c->y() + d->y()) / 4.0;
	double z = (a->z() + b->z() + c->z() + d->z()) / 4.0;

	createIndex(x, y, z);

	nodes.push_back(a);
	nodes.push_back(b);
	nodes.push_back(c);
	nodes.push_back(d);
}

quad::~quad() {}

} /* namespace mesh */
