#ifndef SRC_MESH_HEXAHEDRON_H_
#define SRC_MESH_HEXAHEDRON_H_

#include "primitiveElement.h"

namespace mesh
{

class hexahedron: public virtual primitiveElement
{
public:
	hexahedron(node* a, node* b, node* c, node* d, node* e, node* f, node* g, node* h);

	virtual ~hexahedron();

	// 12	-	VTK_HEXAHEDRON
	int type() const {
		return 12;
	}
};

} /* namespace mesh */

#endif /* SRC_MESH_HEXAHEDRON_H_ */
