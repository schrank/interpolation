#ifndef SRC_MESH_MESHOBJECT_H_
#define SRC_MESH_MESHOBJECT_H_

#include <string>
#include "index.h"

namespace mesh {

class meshObject {
public:
	meshObject() {}

	meshObject(double x, double y, double z) {
		createIndex(x, y, z);
	}

	virtual ~meshObject() {}

	index* getIndex() const {
		return _index;
	}

	bool operator<(const meshObject& ameshObject) const;

protected:
	void createIndex(const double x, const double y, const double z);

protected:
	mesh::index *_index;
};

} /* namespace mesh */

#endif /* SRC_MESH_MESHOBJECT_H_ */
