#ifndef SRC_FEM_FEMNODE_H_
#define SRC_FEM_FEMNODE_H_

#include "../mesh/node.h"

#include <vector>

namespace fem
{

class FEMNode: public virtual mesh::node
{
public:
	FEMNode(double x, double y, double z): mesh::node(x, y, z) {}

	virtual ~FEMNode() {}

	std::vector<std::pair<int, double> > getConstraint() const {
		return _constraint;
	}

	void addConstraint(std::vector<std::pair<int, double> > constraint) {
		_constraint = constraint;
	}

protected:
	std::vector<std::pair<int, double> > _constraint;
};

} /* namespace fem */

#endif /* SRC_FEM_FEMNODE_H_ */
