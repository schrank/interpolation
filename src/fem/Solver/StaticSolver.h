#ifndef SRC_FEM_SOLVER_STATICSOLVER_H_
#define SRC_FEM_SOLVER_STATICSOLVER_H_

#include "Solver.h"

namespace fem
{
namespace solver
{

class StaticSolver: public virtual Solver
{
public:
	StaticSolver() {
		tol = 1e-6;
		max_iter=1000;
	}
	StaticSolver(double nec_tol, int nec_int) {
			tol = nec_tol;
			max_iter=nec_int;
	}

	virtual ~StaticSolver() {
	}

	virtual algebra::algebraVector Solve(
			algebra::algebraMatrix K,
			algebra::algebraMatrix M,
			algebra::algebraMatrix C,
			algebra::algebraVector f);

	virtual std::string getName() const {
		return "Static";
	}

protected:
	double tol;
	uint max_iter;
};

} /* namespace solver */
} /* namespace fem */

#endif /* SRC_FEM_SOLVER_STATICSOLVER_H_ */
