#ifndef SRC_FEM_SOLVER_NEWMARKSOLVER_H_
#define SRC_FEM_SOLVER_NEWMARKSOLVER_H_

#include "Solver.h"

namespace fem
{
namespace solver
{

class NewmarkSolver: public virtual Solver
{
public:
	NewmarkSolver(): _delta_t(1e-6), _delta(0.5), _isReady(false) {
		formConstants();
	}

	NewmarkSolver(
			double delta_t,
			double delta):
				_delta_t(delta_t), _delta(delta), _isReady(false) {
		formConstants();
	}

	virtual ~NewmarkSolver() {}

	virtual algebra::algebraVector Solve(
			algebra::algebraMatrix K,
			algebra::algebraMatrix M,
			algebra::algebraMatrix C,
			algebra::algebraVector f);

	virtual std::string getName() const {
		return "Newmark";
	}

	double getDelta_t() const {
		return _delta_t;
	}

	void setDelta_t(double delta_t) {
		_delta_t = delta_t;
	}

	double getAlpha() const {
		return _alpha;
	}

	double getDelta() const {
		return _delta;
	}

	algebra::algebraVector getFUpdate() const {
		return _fUpdate;
	}

private:
	void formConstants();

private:
	double _delta_t;
	double _alpha;
	double _delta;

	double _a[8];

	algebra::algebraMatrix _eK;
	bool _isReady;

	algebra::algebraVector _u;
	algebra::algebraVector _dotu;
	algebra::algebraVector _ddotu;

	algebra::algebraVector _oldu;
	algebra::algebraVector _olddotu;
	algebra::algebraVector _oldddotu;

	algebra::algebraVector _fUpdate;
};

} /* namespace solver */
} /* namespace fem */

#endif /* SRC_FEM_SOLVER_NEWMARKSOLVER_H_ */
