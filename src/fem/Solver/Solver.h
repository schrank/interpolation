#ifndef SRC_FEM_SOLVER_SOLVER_H_
#define SRC_FEM_SOLVER_SOLVER_H_

#include "../../algebra/algebraMatrix.h"
#include "../../algebra/algebraVector.h"

#include <string>

namespace fem
{
namespace solver
{

class Solver
{
public:
	Solver() {}

	virtual ~Solver() {}

	virtual algebra::algebraVector Solve(
			algebra::algebraMatrix K,
			algebra::algebraMatrix M,
			algebra::algebraMatrix C,
			algebra::algebraVector f) = 0;

	virtual std::string getName() const = 0;

protected:
	std::string _name;
};

} /* namespace solver */
} /* namespace fem */

#endif /* SRC_FEM_SOLVER_SOLVER_H_ */
