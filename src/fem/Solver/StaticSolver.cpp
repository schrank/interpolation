#include "StaticSolver.h"

namespace fem
{
namespace solver
{
algebra::algebraVector StaticSolver::Solve(
		algebra::algebraMatrix M,
		algebra::algebraMatrix K,
		algebra::algebraMatrix C,
		algebra::algebraVector f)
{
	algebra::algebraVector result (M.Rows());
	algebra::algebraVector diagA(M.Rows());			// диагональный предобуславлевател
	algebra::algebraVector r(M.Rows()), Ax(M.Rows()), p(M.Rows()), z(M.Rows()); // вспомогательные вектора

	// вычисление диагонального предобуславлевателя
	for(uint i=0; i < M.Rows(); i++)
		diagA(i) = 1.0 / M(i,i);

	uint k=0;
// ****************************************
// метод сопряжённых градиентов

	//вспомогательные скаляры
	double ro1, ro0, alpha, alpha1, beta, nrmr, nrmb;

	//nrmb = (d_b, d_b)
	nrmb = f.Norm();
	nrmr = nrmb;
	r = f;

	z = diagA.ProductSchur(r);
	p=z;

	// ro1 = (z,r)
	ro1=z*r;

	std::cout<<"nrmb: "<<nrmb<<" ro1: "<<ro1<<std::endl;

	while (((nrmr/nrmb) > tol) && (k <= max_iter ))
	{
		// матрично-векторное произведение  Ax = A * p
		Ax = M*p;

		// alpha1 = (p, Ax)
		alpha1 = p*Ax;
		alpha = ro1 / alpha1;

		// d_res = d_res + alpha * p
		// r = r - alpha * Ax
		result+=alpha*p;
		r-=alpha*Ax;

		// nrmr = (r,r)
		nrmr=r.Norm();

		z = diagA.ProductSchur(r);

		ro0 = ro1 ;
		// ro1 = (z,r)
		ro1=z*r;

		beta = ro1 / ro0;

		p = beta*p;
		p+=z;

		k++;

/*		if (!(k%10000))
			cout << "Iter: " << k << "  Error: " << nrmr/nrmb << endl;*/
		if ((!(k%150))||(k==1))
		{
			std::cout<<k<<" Error:  "<<(nrmr/nrmb)<<" ro=   "<<ro1<<" alpha=   "<<alpha<<std::endl;
		}
	}

// конец метода сопряжённых градиентов
// *******************************************
	std::cout << "Quentity of iterations " << k <<" Error: "<<nrmr/nrmb<< std::endl;

	return result;
}

} /* namespace solver */
} /* namespace fem */
