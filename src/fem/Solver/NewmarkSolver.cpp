#include "../../linearSolver/dpcg.h"
#include "NewmarkSolver.h"

namespace fem
{
namespace solver
{

void NewmarkSolver::formConstants()
{
	_alpha = 0.25 * (0.5 + _delta) * (0.5 + _delta);

	_a[0] = 1.0 / (_alpha * _delta_t * _delta_t);
	_a[1] = _delta / (_alpha * _delta_t);
	_a[2] = 1.0 / (_alpha * _delta_t);
	_a[3] = 1.0 / (2.0 * _alpha) - 1.0;
	_a[4] = _delta / _alpha - 1.0;
	_a[5] = _delta_t * 0.5 * (_delta / _alpha - 2.0);
	_a[6] = _delta_t * (1.0 - _delta);
	_a[7] = _delta_t * _delta;
}

algebra::algebraVector NewmarkSolver::Solve(
		algebra::algebraMatrix K,
		algebra::algebraMatrix M,
		algebra::algebraMatrix C,
		algebra::algebraVector f)
{
	std::cout << "Start Newmark solver" << std::endl;

	uint numeq = K.Rows();

	if ( !_isReady )
	{
		_eK.Resize(numeq, numeq);

		_eK = K + M * _a[0];

		_u.Resize(numeq);
		_dotu.Resize(numeq);
		_ddotu.Resize(numeq);

		_oldu.Resize(numeq);
		_olddotu.Resize(numeq);
		_oldddotu.Resize(numeq);

//		_fUpdate.Resize(numeq);

		_isReady = true;
	}

	f += M * (_a[0] * _u + _a[2] * _dotu + _a[3] * _ddotu);

	ls::dpcg solver(1e-6);

	_oldu = _u;
	_u = solver.solve(_eK, f);

	_oldddotu = _ddotu;
	_ddotu = _a[0] * (_u - _oldu) - _a[2] * _dotu - _a[3] * _oldddotu;

	_olddotu = _dotu;
	_dotu = _olddotu + _a[6] * _oldddotu + _a[7] * _ddotu;

//	std::cout << "Average displacement: " << _u.Average() << std::endl;
//	std::cout << "Average velocity: " << _dotu.Average() << std::endl;
//	std::cout << "Average acceleration: " << _ddotu.Average() << std::endl;

	std::cout << "Norm of displacement: " << _u.Norm() << std::endl;
	std::cout << "Norm of velocity: " << _dotu.Norm() << std::endl;
	std::cout << "Norm of acceleration: " << _ddotu.Norm() << std::endl;

//	_fUpdate = M * (_a[0] * _u + _a[2] * _dotu + _a[3] * _ddotu);

	std::cout << "Finish Newmark solver" << std::endl << std::endl;

	return _u;
}

} /* namespace solver */
} /* namespace fem */
