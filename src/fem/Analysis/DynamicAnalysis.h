#ifndef SRC_FEM_ANALYSIS_DYNAMICANALYSIS_H_
#define SRC_FEM_ANALYSIS_DYNAMICANALYSIS_H_

#include "Analysis.h"

namespace fem
{
namespace analysis
{

class DynamicAnalysis: public virtual Analysis
{
public:
	DynamicAnalysis(): Analysis(), _numsteps(1) {}

	DynamicAnalysis(Domain* domain, solver::Solver* solver):
		Analysis(domain, solver), _numsteps(1) {}

	DynamicAnalysis(Domain* domain, solver::Solver* solver, uint numsteps):
		Analysis(domain, solver), _numsteps(numsteps) {}

	virtual ~DynamicAnalysis() {}

	virtual void Analyze();

protected:
	uint _numsteps;
};

} /* namespace analysis */
} /* namespace fem */

#endif /* SRC_FEM_ANALYSIS_DYNAMICANALYSIS_H_ */
