#include "DynamicAnalysis.h"

#include "../../algebra/algebraMatrix.h"
#include "../../algebra/algebraVector.h"
#include "../Solver/NewmarkSolver.h"

#include <sstream>

namespace fem
{
namespace analysis
{

void DynamicAnalysis::Analyze()
{
	_domain->formStiffnesMassMatrixRVectorQuad();

	uint step = 1;

	while ( step <= _numsteps)
	{
		std::cout << "Start step " << step << std::endl;

		_domain->changeLoad(step, (dynamic_cast<solver::NewmarkSolver*>(_solver))->getDelta_t());

		if ( step > 1 )
			_domain->updateRVectorQuad((dynamic_cast<solver::NewmarkSolver*>(_solver))->getFUpdate());

		_domain->commitState(
			_solver->Solve(
					_domain->getStiffnesMatrix(),
					_domain->getMassMatrix(),
					algebra::algebraMatrix(),
					_domain->getRightHandVector())
				);

		if ( step % 1 == 0 )
		{
			std::stringstream ss;
			ss << "result_" << step;
			_domain->export2vtu(ss.str());
		}

		std::cout << "Finish step " << step << std::endl << std::endl;

		step++;
	}
}

} /* namespace analysis */
} /* namespace fem */
