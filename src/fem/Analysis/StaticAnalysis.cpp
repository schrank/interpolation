#include "StaticAnalysis.h"

namespace fem
{
namespace analysis
{

void StaticAnalysis::Analyze()
{
	_domain->formStiffnesMatrixRVectorQuad();

	_domain->commitState(
			_solver->Solve(
					_domain->getStiffnesMatrix(),
					algebra::algebraMatrix(),
					algebra::algebraMatrix(),
					_domain->getRightHandVector()));
}

} /* namespace analysis */
} /* namespace fem */
