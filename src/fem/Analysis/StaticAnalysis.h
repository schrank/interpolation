#ifndef SRC_FEM_ANALYSIS_STATICANALYSIS_H_
#define SRC_FEM_ANALYSIS_STATICANALYSIS_H_

#include "Analysis.h"

namespace fem
{
namespace analysis
{

class StaticAnalysis: public virtual Analysis
{
public:
	StaticAnalysis(): Analysis(NULL, NULL) {}

	StaticAnalysis(Domain* domain, solver::Solver* solver): Analysis(domain, solver) {}

	virtual ~StaticAnalysis() {}

	virtual void Analyze();
};


} /* namespace analysis */
} /* namespace fem */

#endif /* SRC_FEM_ANALYSIS_STATICANALYSIS_H_ */
