#ifndef SRC_FEM_ANALYSIS_ANALYSIS_H_
#define SRC_FEM_ANALYSIS_ANALYSIS_H_

#include "../Domain.h"
#include "../Solver/Solver.h"

namespace fem
{
namespace analysis
{

class Analysis
{
public:
	Analysis(): _domain(NULL), _solver(NULL) {}

	Analysis(Domain* domain, solver::Solver* solver): _domain(domain), _solver(solver) {}

	virtual ~Analysis() {
		if (!_domain)
			delete _domain;

		if (!_solver)
			delete _solver;
	}

	virtual void Analyze() = 0;

protected:
	Domain* _domain;

	solver::Solver* _solver;
};

} /* namespace analysis */
} /* namespace fem */

#endif /* SRC_FEM_ANALYSIS_ANALYSIS_H_ */
