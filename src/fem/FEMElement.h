#ifndef SRC_FEM_FEMELEMENT_H_
#define SRC_FEM_FEMELEMENT_H_

#include "../mesh/primitiveElement.h"

namespace fem
{

class FEMElement: virtual public mesh::primitiveElement
{
public:
	FEMElement(): _young(1), _poisson(0), _density(1) {}

	virtual ~FEMElement() {}

	void addMaterial(double young, double poisson) {
		_young = young;
		_poisson = poisson;
	}

	void addDensity(double density) {
		_density = density;
	}

	double getYoung() {
		return _young;
	}

	double getPoisson() {
		return _poisson;
	}

	double getDensity() {
		return _density;
	}

protected:
	double _young;
	double _poisson;
	double _density;
};

} /* namespace fem */

#endif /* SRC_FEM_FEMELEMENT_H_ */
