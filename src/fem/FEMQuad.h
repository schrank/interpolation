#ifndef SRC_FEM_FEMQUAD_H_
#define SRC_FEM_FEMQUAD_H_

#include "../mesh/quad.h"
#include "FEMElement.h"
#include "FEMEdge.h"

namespace fem
{

class FEMQuad: public virtual mesh::quad, public virtual FEMElement
{
public:
	FEMQuad(mesh::node* a, mesh::node* b, mesh::node* c, mesh::node* d): mesh::quad(a, b, c, d) {}

 	virtual ~FEMQuad() {}

 	void addLoad(mesh::node* a, mesh::node* b, double p);

 	void changeLoad(mesh::node* a, mesh::node* b, double p);

 	std::vector<FEMEdge*> getEdges() const {
 		return fedges;
 	}

protected:
 	std::vector<FEMEdge*> fedges;
};

} /* namespace fem */

#endif /* SRC_FEM_FEMQUAD_H_ */
