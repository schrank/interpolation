#include "Domain.h"
#include "FEMNode.h"
#include "FEMEdge.h"
#include "FEMQuad.h"

#include <stdexcept>
#include <fstream>
#include <sstream>
#include <cmath>
#include <algorithm>

using namespace mesh;

namespace fem
{

void Domain::export2vtu(std::string file)
{
	const unsigned int numOfNodes = nodes.size();
	const unsigned int numOfElements = elements.size();

	file += ".vtu";

	std::ofstream out(file.c_str());
	if ( out.fail() ) throw std::logic_error ("Mesh::export2vtu: Error with mesh file");

	out << "<?xml version=\"1.0\"?>\n";
	out << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
	out << "  <UnstructuredGrid>\n";
	out << "    <Piece NumberOfPoints=\""<< numOfNodes << "\" NumberOfCells=\"" << numOfElements << "\">\n";

	// координаты точек
	std::string coordinates;
	std::string constrains;
	std::string displacement;
	for (node* aNode : nodes)
	{
		coordinates += to_string(aNode->x()) + " "
						+ to_string(aNode->y()) + " " + to_string(aNode->z()) + " ";

		displacement +=
				  to_string(aNode->dx()) + " "
				+ to_string(aNode->dy()) + " "
				+ to_string(aNode->dz()) + " ";

		double cnstr[] = {-1, -1, -1};

		FEMNode* fn = dynamic_cast<FEMNode*>(aNode);

		std::vector<std::pair<int, double> > cns = fn->getConstraint();

		for (std::pair<int, double> c : cns)
			cnstr[c.first] = c.second;

		for (int j = 0; j < 3; j++)
			constrains += to_string(cnstr[j]) + " ";
	}

	out << "      <PointData Scalars=\"scalars\">\n";
	dataArrayNode(out, constrains, "Int8", "constrains", 3);
	dataArrayNode(out, displacement, "Float64", "displacement", 3);
	out << "      </PointData>\n";

	std::string pressure;
	std::string Young;
	std::string Poisson;
	std::string Density;
	for (primitiveElement* element : elements)
	{
		FEMQuad* fq = dynamic_cast<FEMQuad*>(element);

		double p = 0.0;
		for (FEMEdge* e : fq->getEdges())
			p += e->getP();

		pressure += to_string(p) + " ";

		Young += to_string(fq->getYoung()) + " ";
		Poisson += to_string(fq->getPoisson()) + " ";
		Density += to_string(fq->getDensity()) + " ";
	}

	out << "      <CellData Scalars=\"scalars\">\n";
	dataArrayNode(out, pressure, "Float64", "p", 1);
	dataArrayNode(out, Young, "Float64", "E", 1);
	dataArrayNode(out, Poisson, "Float64", "nu", 1);
	dataArrayNode(out, Density, "Float64", "rho", 1);
	out << "      </CellData>\n";

	out << "      <Points>\n";
	dataArrayNode(out, coordinates, "Float64", "coordinates", 3);
	out << "      </Points>\n";

	// connectivity - список связности элементов
	std::string connectivity;
	// offsets - список индексов начальных координат каждого элемента в списке connectivity
	//
	// например, если все элементы являются четырехугольниками, а список connectivity имеет след. вид:
	// 0 1 2 3 4 5 6 7 0 1 5 4 2 3 7 6 0 4 7 3 1 2 6 5
	// Список offsets будет выглядеть так: 4 8 12 16 20 24
	std::string offsets;
	int offset = 0;

	//types - список типов элементов сетки
	//Двухмерные типы:
	//1		-	VTK_VERTEX
	//3		-	VTK_LINE
	//5		-	VTK_TRIANGLE
	//7		-	VTK_POLYGON(n-угольник)
	//8		-	VTK_PIXEL(квадрат)
	//9		-	VTK_QUAD(четырехугольник)
	//Трехмерные типы:
	//10	-	VTK_TETRA
	//11	-	VTK_VOXEL
	//12	-	VTK_HEXAHEDRON
	//13	-	VTK_WEDGE
	//14	-	VTK_PYRAMID
	std::string types;

	for (primitiveElement* element : elements)
	{
		int numberOfNodes = element->numberOfNodes();

		offset += numberOfNodes;
		offsets += to_string(offset) + " ";

		types += to_string(element->type()) + " ";

		std::vector<node*> nodes = element->getNodes();
		for (node* n : nodes)
		{
			connectivity += to_string((int)findNode(n)) + " ";
		}
	}

	out << "      <Cells>\n";
	dataArrayNode(out, connectivity, "Int64", "connectivity");
	dataArrayNode(out, offsets, "Int64", "offsets");
	dataArrayNode(out, types, "Int64", "types");
	out << "      </Cells>\n";

	out << "    </Piece>\n";
	out << "</UnstructuredGrid>\n";
	out << "</VTKFile>";

	out.close();
}

node* Domain::addNode(double x, double y, double z)
{
	node *aNode = new FEMNode(x, y, z);

	if ( nodes.insert(aNode).second )
		return aNode;

	std::cout << "Node: " << *aNode << "is exist!\n";

	return *nodes.find(aNode);
}

primitiveElement* Domain::addQuad(node* a, node* b, node* c, node* d)
{
	primitiveElement *aQuad = new FEMQuad(a, b, c, d);

	if ( elements.insert(aQuad).second )
		return aQuad;

	std::cout << "Quad: " << *aQuad << "is exist!\n";

	return *elements.find(aQuad);
}

void Domain::commitState(algebra::algebraVector result)
{
	for (node* n : nodes)
	{
		int idx = findNode(n) * 2;

		n->dxdydz(0, result(idx));
		n->dxdydz(1, result(idx + 1));
	}
}

void Gauss(const short num_gp, double **gp, double **w)
{
	double *loc_gp, *loc_w;
	if (num_gp == 1)
	{
		loc_gp = new double[1]; loc_gp[0] = 0;
		loc_w = new double[1]; loc_w[0] = 2;
	}

	if (num_gp == 2)
	{
		loc_gp = new double[2]; loc_gp[0] = -1./sqrt(3.); loc_gp[1] = 1./sqrt(3.);
		loc_w = new double[2]; loc_w[0] = 1.0; loc_w[1] = 1.0;
	}

	if (num_gp == 3)
	{
		loc_gp = new double[3]; loc_gp[0] = -sqrt(3./5.); loc_gp[1] = 0; loc_gp[2] = sqrt(3./5.);
		loc_w = new double[3]; loc_w[0] = 5./9.; loc_w[1] = 8./9.; loc_w[2] = 5./9.;
	}

	*gp = loc_gp;

	*w = loc_w;
}

// An inversion of 3x3 matrix with known determinant.
void inverse2(double (&A)[2][2], double det)
{
	double a[2][2] = {
			 A[1][1], -A[0][1],
			-A[1][0],  A[0][0]
	};

	A[0][0] =  a[0][0] / det; A[0][1] = a[0][1] / det;
	A[1][0] =  a[1][0] / det; A[1][1] = a[1][1] / det;
}

// A determinant of 3x3 matrix.
double det2(double A[2][2])
{
	return A[0][0] * A[1][1] - A[0][1] * A[1][0];
}

double get_Shape(int num, double r0, double s0 )
{
	switch(num)
	{
	case 0:
		return 0.25 * (1 - r0) * (1 - s0);
		break;
	case 1:
		return 0.25 * (1 + r0) * (1 - s0);
		break;
	case 2:
		return 0.25 * (1 + r0) * (1 + s0);
		break;
	case 3:
		return 0.25 * (1 - r0) * (1 + s0);
		break;
	default:
		std::cout << "Error" << std::endl;
	}

	return 0;
}

double dN(double r0, double s0, double q0, int num, int der)
{
/*
	Shape functions:
		case 0: return 0.125 * (1 - r0) * (1 - s0) * (1 - q0);
		case 1: return 0.125 * (1 + r0) * (1 - s0) * (1 - q0);
		case 2: return 0.125 * (1 + r0) * (1 + s0) * (1 - q0);
		case 3: return 0.125 * (1 - r0) * (1 + s0) * (1 - q0);
		case 4: return 0.125 * (1 - r0) * (1 - s0) * (1 + q0);
		case 5: return 0.125 * (1 + r0) * (1 - s0) * (1 + q0);
		case 6: return 0.125 * (1 + r0) * (1 + s0) * (1 + q0);
		case 7: return 0.125 * (1 - r0) * (1 + s0) * (1 + q0);
*/

	if (der == 0)
	{
		switch (num)
		{
			case 0: return 0.125 * -1 * (1 - s0) * (1 - q0);
			case 1: return 0.125 * -1 * (1 + s0) * (1 - q0);
			case 2: return 0.125 * -1 * (1 + s0) * (1 + q0);
			case 3: return 0.125 * -1 * (1 - s0) * (1 + q0);
			case 4: return 0.125 *  1 * (1 - s0) * (1 - q0);
			case 5: return 0.125 *  1 * (1 - s0) * (1 + q0);
			case 6: return 0.125 *  1 * (1 + s0) * (1 + q0);
			case 7: return 0.125 *  1 * (1 + s0) * (1 - q0);
			default: return 0;
		}
	}
	else if (der == 1)
	{
		switch (num)
		{
			case 0: return 0.125 * (1 - r0) *  -1 * (1 - q0);
			case 1: return 0.125 * (1 - r0) *   1 * (1 - q0);
			case 2: return 0.125 * (1 - r0) *   1 * (1 + q0);
			case 3: return 0.125 * (1 - r0) *  -1 * (1 + q0);
			case 4: return 0.125 * (1 + r0) *  -1 * (1 - q0);
			case 5: return 0.125 * (1 + r0) *  -1 * (1 + q0);
			case 6: return 0.125 * (1 + r0) *   1 * (1 + q0);
			case 7: return 0.125 * (1 + r0) *   1 * (1 - q0);
			default: return 0;
		}
	}
	else if (der == 2)
	{
		switch (num)
		{
			case 0: return 0.125 * (1 - r0) * (1 - s0) *  -1;
			case 1: return 0.125 * (1 - r0) * (1 + s0) *  -1;
			case 2: return 0.125 * (1 - r0) * (1 + s0) *   1;
			case 3: return 0.125 * (1 - r0) * (1 - s0) *   1;
			case 4: return 0.125 * (1 + r0) * (1 - s0) *  -1;
			case 5: return 0.125 * (1 + r0) * (1 - s0) *   1;
			case 6: return 0.125 * (1 + r0) * (1 + s0) *   1;
			case 7: return 0.125 * (1 + r0) * (1 + s0) *  -1;
			default: return 0;
		}
	}
	else if (der == 3)
	{
		switch (num)
		{
			case 0: return 0.25 * -1 * (1 - s0);
			case 1: return 0.25 *  1 * (1 - s0);
			case 2: return 0.25 *  1 * (1 + s0);
			case 3: return 0.25 * -1 * (1 + s0);
			default: return 0;
		}
	}
	else if (der == 4)
	{
		switch (num)
		{
			case 0: return 0.25 * (1 - r0) * (-1);
			case 1: return 0.25 * (1 + r0) * (-1);
			case 2: return 0.25 * (1 + r0) * 1;
			case 3: return 0.25 * (1 - r0) * 1;
			default: return 0;
		}
	}
	else return 0;
}

double dN(int num, int der, double dNx[8], double dNy[8], double dNz[8])
{
	if (der == 0 || num == 0)
	{
		return 0;
	}
	else if (der == 1)
	{
		return dNx[num-1];
	}
	else if (der == 2)
	{
		return dNy[num-1];
	}
	else if (der == 3)
	{
		return dNz[num-1];
	}
	else
		return 0;
}

void Jm(double r0, double s0, double n[4][2], double (&J)[2][2])
{
	for (int k = 0; k < 4; k++)
	{
		for (int i = 0; i < 2; i++)
		{
			J[i][0] += n[k][0]*dN(r0,s0,0,k,i+3);
			J[i][1] += n[k][1]*dN(r0,s0,0,k,i+3);
		}
	}
}

void Local_gp(double r0, double s0, double w, double nd[4][2], double D[3][3], double (&k)[8][8])
{
	// Compute Jacobi matrix at point (r0,s0,q0).
/*	double nd[8][3] = {nds[n[0]][0], nds[n[0]][1], nds[n[0]][2],nds[n[1]][0], nds[n[1]][1], nds[n[1]][2],
					   nds[n[2]][0], nds[n[2]][1], nds[n[2]][2],nds[n[3]][0], nds[n[3]][1], nds[n[3]][2],
	                   nds[n[4]][0], nds[n[4]][1], nds[n[4]][2],nds[n[5]][0], nds[n[5]][1], nds[n[5]][2],
	                   nds[n[6]][0], nds[n[6]][1], nds[n[6]][2],nds[n[7]][0], nds[n[7]][1], nds[n[7]][2]};
*/
	// Forming J matrix
	double J[2][2] = {0};
	Jm(r0, s0, nd, J);

	// Compute determinant of J.
	double detJ = det2(J);

	// Inverse J matrix.
	double Ji[2][2] = {J[0][0],J[0][1],J[1][0],J[1][1]};
	inverse2(Ji,detJ);

	// Compute shape functions derivatives in local coordinate system.
	double dNr[4] = {dN(r0,s0,0,0,3),dN(r0,s0,0,1,3),dN(r0,s0,0,2,3),dN(r0,s0,0,3,3)},
	       dNs[4] = {dN(r0,s0,0,0,4),dN(r0,s0,0,1,4),dN(r0,s0,0,2,4),dN(r0,s0,0,3,4)};

	// First computes local stiffness matrix for static problem.
	double B[3][8]={0};

	// Compute shape functions derivatives in global coordinate system.
	double dNx[4], dNy[4];

	for (int i = 0; i < 4; i++)
	{
		dNx[i] = Ji[0][0]*dNr[i]+Ji[0][1]*dNs[i];
		dNy[i] = Ji[1][0]*dNr[i]+Ji[1][1]*dNs[i];
	}

	// Compute Bn matrix.
	B[0][0]=dNx[0]; B[0][2]=dNx[1]; B[0][4]=dNx[2]; B[0][6]=dNx[3];
	B[1][1]=dNy[0]; B[1][3]=dNy[1]; B[1][5]=dNy[2]; B[1][7]=dNy[3];
	B[2][0]=dNy[0]; B[2][1]=dNx[0]; B[2][2]=dNy[1]; B[2][3]=dNx[1];B[2][4]=dNy[2];B[2][5]=dNx[2];B[2][6]=dNy[3];B[2][7]=dNx[3];

/*	// Compute transposed Bn-Matrix.
	for (int i = 0; i < 24; i++)
	{
		Bt[i][0]=B[0][i];Bt[i][1]=B[1][i];Bt[i][2]=B[2][i];Bt[i][3]=B[3][i];Bt[i][4]=B[4][i];Bt[i][5]=B[5][i];
	}
*/
	// Bt*D
	double BtD[8][3];
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			BtD[i][j] = B[0][i]*D[0][j]+B[1][i]*D[1][j]+B[2][i]*D[2][j];
		}
	}

	// BtD * B + "k".
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			k[i][j] += w*fabs(detJ)*(BtD[i][0]*B[0][j]+BtD[i][1]*B[1][j]+BtD[i][2]*B[2][j]);
		}
	}
}

void Local_gp_mass(double r0, double s0, double w, double nd[4][2], double D[3][3], double (&k)[8][8], double (&loc_mass)[8][8], double density)
{
	// Compute Jacobi matrix at point (r0,s0,q0).
/*	double nd[8][3] = {nds[n[0]][0], nds[n[0]][1], nds[n[0]][2],nds[n[1]][0], nds[n[1]][1], nds[n[1]][2],
					   nds[n[2]][0], nds[n[2]][1], nds[n[2]][2],nds[n[3]][0], nds[n[3]][1], nds[n[3]][2],
	                   nds[n[4]][0], nds[n[4]][1], nds[n[4]][2],nds[n[5]][0], nds[n[5]][1], nds[n[5]][2],
	                   nds[n[6]][0], nds[n[6]][1], nds[n[6]][2],nds[n[7]][0], nds[n[7]][1], nds[n[7]][2]};
*/
	// Forming J matrix
	double J[2][2]={0};
	Jm(r0,s0,nd,J);

	// Compute determinant of J.
	double detJ = det2(J);

	// Inverse J matrix.
	double Ji[2][2] = {J[0][0],J[0][1],J[1][0],J[1][1]};
	inverse2(Ji,detJ);

	// Compute matrix shape functions
	double N[2][8];
	for(int i=0; i<4; i++) /// ?????????? kim
	{
		N[0][2*i] = get_Shape(i,r0,s0);
		N[1][2*i+1] = get_Shape(i,r0,s0);

		/////////////////////////// kim
		N[1][2*i] = 0.0;
		N[0][2*i+1] = 0.0;
		/////////////////////////// kim
	}

	// Compute shape functions derivatives in local coordinate system.
	double dNr[4] = {dN(r0,s0,0,0,3),dN(r0,s0,0,1,3),dN(r0,s0,0,2,3),dN(r0,s0,0,3,3)},
	       dNs[4] = {dN(r0,s0,0,0,4),dN(r0,s0,0,1,4),dN(r0,s0,0,2,4),dN(r0,s0,0,3,4)};

	// First computes local stiffness matrix for static problem.
	double B[3][8]={0};

	// Compute shape functions derivatives in global coordinate system.
	double dNx[4], dNy[4];

	for (int i = 0; i < 4; i++)
	{
		dNx[i] = Ji[0][0]*dNr[i]+Ji[0][1]*dNs[i];
		dNy[i] = Ji[1][0]*dNr[i]+Ji[1][1]*dNs[i];
	}

	// Compute Bn matrix.
	B[0][0]=dNx[0]; B[0][2]=dNx[1]; B[0][4]=dNx[2]; B[0][6]=dNx[3];
	B[1][1]=dNy[0]; B[1][3]=dNy[1]; B[1][5]=dNy[2]; B[1][7]=dNy[3];
	B[2][0]=dNy[0]; B[2][1]=dNx[0]; B[2][2]=dNy[1]; B[2][3]=dNx[1];B[2][4]=dNy[2];B[2][5]=dNx[2];B[2][6]=dNy[3];B[2][7]=dNx[3];

/*	// Compute transposed Bn-Matrix.
	for (int i = 0; i < 24; i++)
	{
		Bt[i][0]=B[0][i];Bt[i][1]=B[1][i];Bt[i][2]=B[2][i];Bt[i][3]=B[3][i];Bt[i][4]=B[4][i];Bt[i][5]=B[5][i];
	}
*/
	// Bt*D
	double BtD[8][3];
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			BtD[i][j] = B[0][i]*D[0][j]+B[1][i]*D[1][j]+B[2][i]*D[2][j];
		}
	}

	// BtD * B + "k".
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			k[i][j] += w*detJ*(BtD[i][0]*B[0][j]+BtD[i][1]*B[1][j]+BtD[i][2]*B[2][j]);
		}
	}

	//loc_mass = NtN
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			loc_mass[i][j] = 0;

	//!!!!!  N = 8x2 а тут циклы по 8x8
	for (int i = 0; i < 8; i++)
		for (int k = 0; k < 2; k++)
			for (int j = 0; j < 8; j++)
//				loc_mass[i][j]+= N[i][k] * N[k][j]; /// kim
				loc_mass[i][j]+= N[k][i] * N[k][j]; /// kim ??????????

	//loc_mas*=Weights*weights*density
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			loc_mass[i][j] *= fabs(detJ)*w*density; // nns??? |detJ| 25/06/2019

}

void Domain::formStiffnesMatrixRVectorQuad()
{
	double *gp, *w;
	const short num_gp=3;
	Gauss(num_gp, &gp, &w);

	_StiffnesMatrix.Resize((unsigned int)nodes.size()*2, (unsigned int)nodes.size()*2);
	_F.Resize((unsigned int)nodes.size()*2);

	for (mesh::primitiveElement* it_element : elements)
	{
		FEMQuad *element = dynamic_cast <FEMQuad*> (it_element);
		double k[8][8]={0};
		double resist[8]={0};
		double nd[4][2];
		uint i=0;
		for(mesh::node* it_node : element->getNodes())
		{
			nd[i][0]=it_node->x();
			nd[i][1]=it_node->y();
			i++;
		}

		double D[3][3];
		//формирование матрицы материалов D
		double E = element->getYoung(), v = element->getPoisson(), a = (E*(1-v))/((1+v)*(1-2*v));

		D[0][0]=  		 a; D[0][1]= a*v/(1-v); D[0][2]=    0;
		D[1][0]= a*v/(1-v); D[1][1]= 		 a; D[1][2]=    0;
		D[2][0]=    	 0; D[2][1]=    	 0; D[2][2]=a*(1-2*v)/(2*(1-v));

		//double rho = ;

		for (uint i = 0; i < num_gp; i++)
		{
			for (uint j = 0; j < num_gp; j++)
			{
				Local_gp(gp[i], gp[j], w[i] * w[j], nd, D, k);
			}
		}

		i=0;
		for(FEMEdge* it_edge : element->getEdges())
		{
			double x1 = it_edge->getNode(0)->x();
			double x2 = it_edge->getNode(1)->x();

			double y1 = it_edge->getNode(0)->y();
			double y2 = it_edge->getNode(1)->y();

			double xn = y2 - y1;
			double yn = x1 - x2;
			double l = sqrt(xn * xn + yn * yn); //норма нормали и длина ребра
			xn = xn / l;
			yn = yn / l;

//			double l = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)); //длина ребра

			std::vector<node*> enodes = element->getNodes();

			int index = find(enodes.begin(), enodes.end(), it_edge->getNode(0)) - enodes.begin();

			resist[2*index] = l/2*it_edge->getP()*xn;
			resist[2*index+1] = l/2*it_edge->getP()*yn;

			index = find (enodes.begin(), enodes.end(), it_edge->getNode(1)) - enodes.begin();
			resist[2*index] = l/2*it_edge->getP()*xn;
			resist[2*index+1] = l/2*it_edge->getP()*yn;
		}

		i = 0; uint j = 0;
		for(mesh::node* it_node : element->getNodes())
		{
			j = 0;
			for(mesh::node* it_node2 : element->getNodes())
			{
				_StiffnesMatrix(2 * findNode(it_node), 2 * findNode(it_node2)) += k[2 * i][2 * j];
				_StiffnesMatrix(2 * findNode(it_node) + 1, 2 * findNode(it_node2)) += k[2 * i + 1][2 * j];

				_StiffnesMatrix(2 * findNode(it_node), 2 * findNode(it_node2) + 1) += k[2 * i][2 * j + 1];
				_StiffnesMatrix(2 * findNode(it_node) + 1, 2 * findNode(it_node2) + 1) += k[2 * i + 1][2 * j + 1];
				j++;
			}

			_F(2 * findNode(it_node)) += resist[2 * i];
			_F(2 * findNode(it_node) + 1) += resist[2 * i + 1];

			i++;
		}
	}

	for(mesh::node* it_node : nodes)
	{
		FEMNode* node1 = dynamic_cast <FEMNode*> (it_node);
		std::vector<std::pair<int, double> > constraint = node1->getConstraint();

		unsigned int dof = findNode(node1);

		if (constraint.size())
		{
			for(std::pair<int, double> it : constraint)
			{
				_StiffnesMatrix( dof * 2 + it.first, dof * 2 + it.first) = 1;

				_F(dof * 2 + it.first) = it.second;

				for(uint i = 0; i < _StiffnesMatrix.Rows(); i++)
				{
					if ( (dof * 2 + it.first ) != i )
					{
						_StiffnesMatrix( dof * 2 + it.first, i) = 0;

						_F(i) -= _StiffnesMatrix(i, dof * 2 + it.first) * it.second;

						_StiffnesMatrix(i , dof * 2 + it.first) = 0;
					}
				}
			}
		}
	}
}

void Domain::formStiffnesMassMatrixRVectorQuad()
{
	double *gp, *w;
	const short num_gp=3;
	Gauss(num_gp, &gp, &w);

	_StiffnesMatrix.Resize((unsigned int)nodes.size() * 2, (unsigned int)nodes.size() * 2);
	_MassMatrix.Resize((unsigned int)nodes.size() * 2, (unsigned int)nodes.size() * 2);
	_F.Resize((unsigned int)nodes.size() * 2);

	for (mesh::primitiveElement* it_element : elements)
	{
		FEMQuad *element = dynamic_cast <FEMQuad*> (it_element);
		double k[8][8]={0};
		double resist[8]={0};
		double nd[4][2];
		double loc_mass[8][8];
		uint i = 0;
		for(mesh::node* it_node : element->getNodes())
		{
			nd[i][0]=it_node->x();
			nd[i][1]=it_node->y();
			i++;
		}

		double D[3][3];
		//формирование матрицы материалов D
		double E = element->getYoung(), v = element->getPoisson(), a = (E*(1-v))/((1+v)*(1-2*v));
		double density = element->getDensity();

		D[0][0]=  		 a; D[0][1]= a*v/(1-v); D[0][2]=    0;
		D[1][0]= a*v/(1-v); D[1][1]= 		 a; D[1][2]=    0;
		D[2][0]=    	 0; D[2][1]=    	 0; D[2][2]=a*(1-2*v)/(2*(1-v));

		//double rho = ;

		for (uint i = 0; i < num_gp; i++)
		{
			for (uint j = 0; j < num_gp; j++)
			{
				Local_gp_mass(gp[i],gp[j],w[i]*w[j],nd,D,k,loc_mass,density);
			}
		}

		for (int i = 0; i < 8; i++)
		{
			double sum = 0.0;
			for (int j = 0; j < 8; j++)
				sum += loc_mass[i][j];

			loc_mass[i][i] = sum;
		}

		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++)
				if( i != j)
					loc_mass[i][j] = 0;

		i = 0;
		for(FEMEdge* it_edge : element->getEdges())
		{
			double x1 = it_edge->getNode(0)->x();
			double x2 = it_edge->getNode(1)->x();

			double y1 = it_edge->getNode(0)->y();
			double y2 = it_edge->getNode(1)->y();

			double xn = y2 - y1;
			double yn = x1 - x2;
			double l = sqrt(xn * xn + yn * yn); //норма нормали и длина ребра
			xn = xn / l;
			yn = yn / l;

//			double l = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)); //длина ребра

			std::vector<node*> enodes = element->getNodes();

			int index = find(enodes.begin(), enodes.end(), it_edge->getNode(0)) - enodes.begin();

			resist[2 * index] = l / 2.0 * it_edge->getP() * xn;
			resist[2 * index + 1] = l / 2.0 * it_edge->getP() * yn;

			index = find(enodes.begin(), enodes.end(), it_edge->getNode(1)) - enodes.begin();
			resist[2 * index] = l / 2.0 * it_edge->getP() * xn;
			resist[2 * index + 1] = l / 2.0 * it_edge->getP() * yn;
		}

		i = 0;
		uint j = 0;
		for(mesh::node* it_node : element->getNodes())
		{
			j = 0;
			for(mesh::node* it_node2 : element->getNodes())
			{
				_StiffnesMatrix(2*findNode(it_node) , 2*findNode(it_node2)) += k[2*i][2*j];
				_StiffnesMatrix(2*findNode(it_node)+1 , 2*findNode(it_node2)) += k[2*i+1][2*j];
				_StiffnesMatrix(2*findNode(it_node) , 2*findNode(it_node2)+1) += k[2*i][2*j+1];
				_StiffnesMatrix(2*findNode(it_node)+1 , 2*findNode(it_node2)+1) += k[2*i+1][2*j+1];

				_MassMatrix(2*findNode(it_node) , 2*findNode(it_node2)) += loc_mass[2*i][2*j];
				_MassMatrix(2*findNode(it_node)+1 , 2*findNode(it_node2)) += loc_mass[2*i+1][2*j];
				_MassMatrix(2*findNode(it_node) , 2*findNode(it_node2)+1) += loc_mass[2*i][2*j+1];
				_MassMatrix(2*findNode(it_node)+1 , 2*findNode(it_node2)+1) += loc_mass[2*i+1][2*j+1];

				j++;
			}

			_F(2*findNode(it_node))+=resist[2*i];
			_F(2*findNode(it_node)+1)+=resist[2*i+1];

			i++;
		}
	}

	for(mesh::node* it_node : nodes)
	{
		FEMNode* node1 = dynamic_cast <FEMNode*> (it_node);
		std::vector<std::pair<int, double> > constraint = node1->getConstraint();
		if (constraint.size())
		{
			for(std::pair<int, double> it : constraint)
			{
				_StiffnesMatrix(findNode(node1)*2+it.first , findNode(node1)*2+it.first) = 1;
				_MassMatrix(findNode(node1)*2+it.first , findNode(node1)*2+it.first) = 1; //граничные условия для матрицы масс на диагонале 1
				_F(findNode(node1)*2+it.first) = it.second;
				for(uint i=0; i<_StiffnesMatrix.Rows(); i++)
				{
					if ((findNode(node1)*2+it.first)!=i)
					{
						_StiffnesMatrix(findNode(node1)*2+it.first , i)=0;
						_F(i)-= _StiffnesMatrix(i , findNode(node1)*2+it.first)*it.second;
						_StiffnesMatrix(i , findNode(node1)*2+it.first)=0;
					}
				}
			}
		}
	}
}
void Domain::updateRVectorQuad(algebra::algebraVector fUdpate)
{
	_F.Zero();

	for (mesh::primitiveElement* it_element : elements)
	{
		FEMQuad *element = dynamic_cast <FEMQuad*> (it_element);
		double resist[8]={0};

		uint i = 0;
		for(FEMEdge* it_edge : element->getEdges())
		{
			double x1 = it_edge->getNode(0)->x();
			double x2 = it_edge->getNode(1)->x();

			double y1 = it_edge->getNode(0)->y();
			double y2 = it_edge->getNode(1)->y();

			double xn = y2 - y1;
			double yn = x1 - x2;
			double l = sqrt(xn * xn + yn * yn); //норма нормали и длина ребра
			xn = xn / l;
			yn = yn / l;

//			double l = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)); //длина ребра

			std::vector<node*> enodes = element->getNodes();

			int index = find(enodes.begin(), enodes.end(), it_edge->getNode(0)) - enodes.begin();

			resist[2*index] = l/2*it_edge->getP()*xn;
			resist[2*index+1] = l/2*it_edge->getP()*yn;

			index = find (enodes.begin(), enodes.end(), it_edge->getNode(1)) - enodes.begin();
			resist[2*index] = l/2*it_edge->getP()*xn;
			resist[2*index+1] = l/2*it_edge->getP()*yn;
		}

		i = 0;
		for(mesh::node* it_node : element->getNodes())
		{
			_F(2*findNode(it_node))+=resist[2*i];
			_F(2*findNode(it_node)+1)+=resist[2*i+1];
			i++;
		}
	}

//	_F += fUdpate;

////!!!!
	// constarint
	for(mesh::node* it_node : nodes)
	{
		FEMNode* node1 = dynamic_cast <FEMNode*> (it_node);
		std::vector<std::pair<int, double> > constraint = node1->getConstraint();

		if (constraint.size())
			for(std::pair<int, double> it : constraint)
				_F(findNode(node1)*2+it.first) = it.second;
	}

}

void Domain::changeLoad(uint step, double delta_t)
{
	for (primitiveElement* e :getElements())
	{
		FEMQuad* fq = dynamic_cast<FEMQuad*>(e);

		double dp = 100 * delta_t * step + 0.004;

		for (FEMEdge* e : fq->getEdges())
		{
			if (std::fabs(e->centreOfMass().y() - 0.02) < 1e-6 && e->centreOfMass().x() < dp )
				e->changeP(-17e-7);
		}
	}
}

//void FEMDomain::checkDeltaT()
//{
//	if (getDimension() == 3)
//	{
//		double edge_min = 1e50;
//		for (Mesh_::ObjectIterator iter = getEdgeIterator(_begin_); iter != _end_; iter++)
//		{
//			handle<Mesh_Edge> edge = *iter;
//			if (edge->Length() < edge_min)
//				edge_min = edge->Length();
//		}
//
//		double A_max = -1e50;
//		double L_min = 1e50;
//		for (Mesh_::ObjectIterator iter = getCellIterator(_begin_); iter != _end_; iter++)
//		{
//			handle<Mesh_Cell> cell = *iter;
//			for (unsigned i = 0; i < cell->NumberOfPolygons(); i++)
//				if (A_max < cell->getPolygon(i)->Area())
//					A_max = cell->getPolygon(i)->Area();
//
//			double Le = 3 * cell->Volume() / A_max;
//			if (L_min > Le)
//				L_min = Le;
//		}
//
//		double c_max = -1e50;
//		for (Mesh_::ObjectIterator iter = getElementIterator(_begin_); iter != _end_; iter++)
//		{
//			handle<FEMMaterial> material = handle<FEMMaterial>((*iter)->getProperty(Prototype_Material_Type()));
//			handle<Prototype_Density> density = handle<Prototype_Density>((*iter)->getProperty(Prototype_Density_Type()));
//
//			if (material.IsNull() || density.IsNull() )
//			{
//				cout << "No material or density property!" << endl;
//				return;
//			}
//			else
//			{
//				double E = material->getYoungModulus();
//				double nu = material->getPoissonRatio();
//
//				double c = sqrt( (E * (1 - nu) ) / (density->getDensity() * (1 + nu) * (1 - 2 * nu)) );
//
//				if (c > c_max)
//					c_max = c;
//			}
//		}
//
//		cout << "deltaT must be less then " << max(edge_min / c_max, L_min / c_max) << endl;
//	}
//	else
//		logic_error("Dimension not equal 3!");
//}

} /* namespace fem */
