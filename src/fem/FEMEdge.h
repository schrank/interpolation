#ifndef SRC_FEM_FEMEDGE_H_
#define SRC_FEM_FEMEDGE_H_

#include "../mesh/edge.h"

namespace fem
{

class FEMEdge: public virtual mesh::edge
{
public:
	FEMEdge(mesh::node* a, mesh::node* b): mesh::edge(a, b), _p(0) {}

	virtual ~FEMEdge() {
	}

	double getP() const {
		return _p;
	}

	void addP(double p) {
		_p = p;
	}

	void changeP(double p) {
		_p = p;
	}

protected:
	double _p;
};

} /* namespace fem */

#endif /* SRC_FEM_FEMEDGE_H_ */
