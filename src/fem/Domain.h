#ifndef SRC_FEM_DOMAIN_H_
#define SRC_FEM_DOMAIN_H_

#include "../mesh/Mesh.h"
#include "../algebra/algebraMatrix.h"
#include "../algebra/algebraVector.h"

namespace fem
{

class Domain: public virtual mesh::Mesh
{
public:
	Domain() {}

	virtual ~Domain() {}

	virtual mesh::node* addNode(double x, double y, double z);

	virtual mesh::primitiveElement* addQuad(mesh::node* a, mesh::node* b, mesh::node* c, mesh::node* d);

	virtual void export2vtu(std::string file = "mesh");

	void formStiffnesMassMatrixRVectorQuad();

	void updateRVectorQuad(algebra::algebraVector fUdpate = algebra::algebraVector());

	void formStiffnesMatrixRVectorQuad();

	algebra::algebraMatrix getStiffnesMatrix() const {
		return _StiffnesMatrix;
	}

	algebra::algebraMatrix getMassMatrix() const {
		return _MassMatrix;
	}

	algebra::algebraVector getRightHandVector() const {
		return _F;
	}

	void commitState(algebra::algebraVector result);

	void changeLoad(uint step, double delta_t);

protected:
	algebra::algebraMatrix _StiffnesMatrix;
	algebra::algebraMatrix _MassMatrix;
	algebra::algebraVector _F;
};

} /* namespace fem */

#endif /* SRC_FEM_DOMAIN_H_ */
