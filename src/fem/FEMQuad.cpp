#include "FEMQuad.h"

#include <algorithm>

namespace fem
{

void FEMQuad::addLoad(mesh::node* a, mesh::node* b, double p)
{
	FEMEdge* e = new FEMEdge(a, b);

	if (find (fedges.begin(), fedges.end(), e) == fedges.end())
	{
		e->addP(p);
		fedges.push_back(e);
	}
	else
	    std::cout << "Edge already have load: " << e->getP() <<  std::endl;
}

void FEMQuad::changeLoad(mesh::node* a, mesh::node* b, double p)
{
	FEMEdge* e = new FEMEdge(a, b);

	std::vector<FEMEdge*>::iterator ef = find (fedges.begin(), fedges.end(), e);

	if (ef != fedges.end())
	{
		std::cout << "Edge already have load: " << e->getP() <<  std::endl;
		((FEMEdge*)*ef)->changeP(p);
	}
	else
	{
		e->addP(p);
		fedges.push_back(e);
	}
}


} /* namespace fem */
