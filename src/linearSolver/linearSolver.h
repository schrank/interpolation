/*
 * linearSolver.h
 *
 *  Created on: Oct 20, 2018
 *      Author: kim
 */

#ifndef SRC_LINEARSOLVER_LINEARSOLVER_H_
#define SRC_LINEARSOLVER_LINEARSOLVER_H_

namespace ls {

class linearSolver {
public:
	linearSolver();
	virtual ~linearSolver();
};

} /* namespace ls */

#endif /* SRC_LINEARSOLVER_LINEARSOLVER_H_ */
