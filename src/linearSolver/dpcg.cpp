#include "dpcg.h"

#include <iostream>
#include <cmath>

using namespace algebra;
using namespace std;

extern int num_threads;

namespace ls {

algebraVector omp_MatrixVectorProduct(algebraMatrix A, algebraVector b)
{
	algebraVector res(b.Size());

#pragma omp parallel for num_threads(num_threads)
		for (uint i = 0; i < A.Rows(); i++)
		{
			double sum = 0.0;
			for (uint j = 0; j < A.Cols(); j++)
				sum += b(j) * A(i, j);

			res(i) = sum;
		}

	return res;
}

double omp_VectorVectorProduct(algebraVector a, algebraVector b)
{
	algebraVector res(b.Size());

	double sum = 0.0;
#pragma omp parallel for reduction(+:sum) num_threads(num_threads)
	for (uint i = 0; i < a.Size(); i++)
	{
		sum += a(i) * b(i);
	}

	return sum;
}

algebraVector omp_axpy(double a, algebraVector x, double p, algebraVector y)
{
	algebraVector res(x.Size());

#pragma omp parallel for num_threads(num_threads)
	for (uint i = 0; i < x.Size(); i++)
	{
		res(i) = a * x(i) + p * y(i);
	}

	return res;
}


algebraVector dpcg::solve(algebraMatrix A, algebraVector b)
{
	cout << endl << "  Start DPCG solver" << endl;

	// number of equations
	unsigned int noe = b.Size();

	algebraVector r(b);
	algebraVector p(noe);
	algebraVector x(noe);
	algebraVector z(noe);
	algebraVector M(noe);

	cout.flags(ios::scientific | ios::left | ios::dec);
	cout.precision(5);

#pragma omp parallel for num_threads(num_threads)
	for	(unsigned int i = 0; i < noe; i++)
		if ( A(i, i) != 0.0 )
			M(i) = 1.0 / A(i, i);
		else
			M(i) = 1.0;

#pragma omp parallel for num_threads(num_threads)
	for	(unsigned int i = 0; i < noe; i++)
		z(i) = M(i) * r(i);

	p = z;

//	double rho = r * z;
	double rho = omp_VectorVectorProduct(r, z);

//	double error = r.Norm();
	double error = sqrt(omp_VectorVectorProduct(r, r));

	unsigned int step = 1;

	double r0;

	algebraVector Ap(noe);

	while (error > _precision && step < 10 * noe)
	{
		// Ap = A * p;
		Ap = omp_MatrixVectorProduct(A, p);

		// double alpha = (r * z) / (p * Ap);
		 double alpha = omp_VectorVectorProduct(r, z) / omp_VectorVectorProduct(p, Ap);


//		x += p * alpha;
		x = omp_axpy(1.0, x, alpha, p);

//		r -= Ap * alpha;
		r = omp_axpy(1.0, r, -alpha, Ap);


#pragma omp parallel for num_threads(num_threads)
		for	(unsigned int i = 0; i < noe; i++)
			z(i) = M(i) * r(i);

		double oldrho = rho;

//		rho = z * r;
		rho = omp_VectorVectorProduct(z, r);

		double beta = rho / oldrho;

		//p = z + p * beta;
		p = omp_axpy(1.0, z, beta, p);

		if (step == 1)
//			r0 = r.Norm();
			r0 = sqrt(omp_VectorVectorProduct(r, r));

//		error = r.Norm() / r0;
		error = sqrt(omp_VectorVectorProduct(r, r)) / r0;

		if ( step % 100 == 0.0 )
		{
			cout << "       step " << step << " error: ";
			if ( error > 1.0e18 )
				cout << " > 1.0e18 " << endl;
			else
				cout << error << endl;
		}
		step++;
	}
	cout.precision(5);
	cout.flags(ios::scientific);
	cout << "     Error: " << error << endl;
	cout << "     Number Of Iterations: " << --step << endl;
	cout << "     SOE Size: " << x.Size() << endl;
	cout << "  End DPCG Solver" << endl << endl;

	return x;
}


} /* namespace ls */
