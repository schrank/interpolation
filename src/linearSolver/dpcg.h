#ifndef SRC_LINEARSOLVER_DPCG_H_
#define SRC_LINEARSOLVER_DPCG_H_

#include "linearSolver.h"

#include "../algebra/algebraVector.h"
#include "../algebra/algebraMatrix.h"

namespace ls {

class dpcg: public virtual linearSolver {
public:
	dpcg(): _precision(1e-8) {}

	dpcg(double precision): _precision(precision) {}

	virtual ~dpcg() {}

	algebra::algebraVector solve(algebra::algebraMatrix M, algebra::algebraVector v);

private:
	double _precision;
};

} /* namespace ls */

#endif /* SRC_LINEARSOLVER_DPCG_H_ */
