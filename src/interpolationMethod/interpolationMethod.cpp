#include "interpolationMethod.h"

namespace im {

double distance(mesh::node* a, mesh::node* b)
{
	return std::sqrt(
			(b->x() - a->x()) * (b->x() - a->x()) +
			(b->y() - a->y()) * (b->y() - a->y()) +
			(b->z() - a->z()) * (b->z() - a->z()));
}


interpolationMethod::interpolationMethod() {
	// TODO Auto-generated constructor stub

}

interpolationMethod::~interpolationMethod() {
	// TODO Auto-generated destructor stub
}

} /* namespace im */
