/*
 * tfi.h
 *
 *  Created on: Oct 20, 2018
 *      Author: kim
 */

#ifndef SRC_INTERPOLATIONMETHOD_TFI_H_
#define SRC_INTERPOLATIONMETHOD_TFI_H_

#include "interpolationMethod.h"
#include <vector>


namespace im {

class tfi: public virtual interpolationMethod {
public:
	tfi();
	virtual ~tfi();
	std::vector<mesh::node*> interpolate(std::vector<mesh::node*> x, std::vector<mesh::node*> y);
	std::vector<mesh::node*> deformation(std::vector<mesh::node*> Left, std::vector<mesh::node*> Right);

};


} /* namespace im */

#endif /* SRC_INTERPOLATIONMETHOD_TFI_H_ */
