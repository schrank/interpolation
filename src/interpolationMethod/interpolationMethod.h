#ifndef SRC_INTERPOLATIONMETHOD_INTERPOLATIONMETHOD_H_
#define SRC_INTERPOLATIONMETHOD_INTERPOLATIONMETHOD_H_

#include <cmath>
#include "../mesh/node.h"

namespace im {

double distance(mesh::node* a, mesh::node* b);

class interpolationMethod {
public:
	interpolationMethod();
	virtual ~interpolationMethod();
};

} /* namespace im */

#endif /* SRC_INTERPOLATIONMETHOD_INTERPOLATIONMETHOD_H_ */
