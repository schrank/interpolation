#ifndef SRC_INTERPOLATIONMETHOD_RBF_H_
#define SRC_INTERPOLATIONMETHOD_RBF_H_

#include <vector>
#include "interpolationMethod.h"

namespace im {

class rbf: public virtual interpolationMethod {
public:
	rbf();

	virtual ~rbf();

	std::vector<mesh::node*> interpolate(std::vector<mesh::node*> x, std::vector<mesh::node*> y);

	std::vector<mesh::node*> deformation(std::vector<mesh::node*> Left, std::vector<mesh::node*> Right);

	std::vector<std::vector<mesh::node*> > deformation(std::vector<mesh::node*> Left, std::vector<std::vector<mesh::node*> > Right);

	std::vector<std::vector<mesh::node*> > deformation(std::vector<std::vector<mesh::node*> > Left, std::vector<std::vector<mesh::node*> > Right);
};

} /* namespace im */

#endif /* SRC_INTERPOLATIONMETHOD_RBF_H_ */
