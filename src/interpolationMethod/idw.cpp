#include "idw.h"
#include "../algebra/algebraMatrix.h"
#include <omp.h>
#include <cmath>

using namespace mesh;
using namespace algebra;
extern int num_threads;// = omp_get_max_threads();

namespace im {

std::vector<node*> idw::interpolate(std::vector<node*> Left, std::vector<node*> Right)
{
	algebraMatrix M(Right.size(), Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned i = 0; i < Right.size(); i++)
		for (unsigned j = 0; j < Left.size(); j++)
		{
			double d = im::distance(Right[i], Left[j]);

			for( unsigned int p = 1; p < _p; p++ )
				d *= d;

			if ( d != 0.0 )
				M(i, j) = 1.0 / d;
			else
				M(i, j) = 1.0;
		}

	algebraVector data(Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
		data(i) = Left[i]->data();

	for (unsigned i = 0; i < M.Rows(); i++)
	{
		double sumRows = M.SumRow(i);

		if ( sumRows != 0 )
			Right[i]->data( (M.Row(i) * data) / sumRows );
		else
			Right[i]->data(0.0);
	}

	return Right;
}

std::vector<std::vector<node*> > idw::deformation(std::vector<std::vector<mesh::node*> > Left, std::vector<std::vector<node*> > Right)
{
	const unsigned int dim = (Right.size() == Left.size()) ? Right.size() : 0;

	algebraMatrix* M = new algebraMatrix[dim];
	for (unsigned int i = 0; i < dim; i++)
		M[i].Resize(Right[i].size(), Left[i].size());

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Right[i].size(); j++)
			for (unsigned int k = 0; k < Left[i].size(); k++)
			{
				double d = distance(Right[i][j], Left[i][k]);

				for( unsigned int p = 1; p < _p; p++ )
					d *= d;

				if ( d != 0.0 )
					M[i](j, k) = 1.0 / d;
				else
					M[i](j, k) = 1.0;
			}

	algebraVector* d = new algebraVector[dim];
	for (unsigned int i = 0; i < dim; i++)
		d[i].Resize(Left[i].size());

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Left[i].size(); j++)
			d[i](j) = Left[i][j]->di(i);

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned j = 0; j < M[i].Rows(); j++)
		{
			double sumRows = M[i].SumRow(j);

			if ( sumRows != 0 )
			{
				if ( d[i].Norm() )
					Right[i][j]->dxdydz(i, (M[i].Row(j) * d[i]) / sumRows);
			}
		}

	return Right;
}


std::vector<node*> idw::deformation(std::vector<node*> Left, std::vector<node*> Right)
{
	algebraMatrix M(Right.size(), Left.size());
#pragma omp parallel for num_threads(num_threads)
	for (unsigned i = 0; i < Right.size(); i++)
		for (unsigned j = 0; j < Left.size(); j++)
		{
			double d = distance(Right[i], Left[j]);

			for( unsigned int p = 1; p < _p; p++ )
				d *= d;

			if ( d != 0.0 )
				M(i, j) = 1.0 / d;
			else
				M(i, j) = 1.0;
		}

	algebraVector dx(Left.size());
	algebraVector dy(Left.size());
	algebraVector dz(Left.size());
#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
	{
		dx(i) = Left[i]->dx();
		dy(i) = Left[i]->dy();
		dz(i) = Left[i]->dz();
	}

	for (unsigned i = 0; i < M.Rows(); i++)
	{
		double sumRows = M.SumRow(i);

		double tdx = 0.0, tdy = 0.0, tdz = 0.0;

		if ( sumRows != 0 )
		{
			if ( dx.Norm() )
				tdx = (M.Row(i) * dx) / sumRows;
			if ( dy.Norm() )
				tdy = (M.Row(i) * dy) / sumRows;
			if ( dz.Norm() )
				tdz = (M.Row(i) * dz) / sumRows;
		}

		Right[i]->dxdydz(tdx, tdy, tdz);
	}

	return Right;
}

} /* namespace im */
