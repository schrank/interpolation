#ifndef SRC_INTERPOLATIONMETHOD_IDW_H_
#define SRC_INTERPOLATIONMETHOD_IDW_H_

#include <vector>
#include "interpolationMethod.h"

namespace im {

class idw: public virtual interpolationMethod {
public:
	idw(unsigned int p): _p(p) {}

	virtual ~idw() {}

	std::vector<mesh::node*> interpolate(std::vector<mesh::node*> Left, std::vector<mesh::node*> Right);

	std::vector<mesh::node*> deformation(std::vector<mesh::node*> Left, std::vector<mesh::node*> Right);

	std::vector<std::vector<mesh::node*> > deformation(std::vector<std::vector<mesh::node*> > Left, std::vector<std::vector<mesh::node*> > Right);
private:
	unsigned int _p;
};

} /* namespace im */

#endif /* SRC_INTERPOLATIONMETHOD_IDW_H_ */
