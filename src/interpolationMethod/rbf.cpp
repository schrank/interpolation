#include "rbf.h"
#include "../algebra/algebraMatrix.h"
#include "../linearSolver/dpcg.h"

#include <omp.h>
#include <cmath>
#include <fstream>


using namespace mesh;
using namespace algebra;
using namespace std;

extern int num_threads;// = omp_get_max_threads();

namespace im {


rbf::rbf() {
}

rbf::~rbf() {

}


vector<node*> rbf::interpolate(vector<node*> Left, vector<node*> Right)
{
	algebraMatrix L(Left.size(), Left.size());


#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
		for (unsigned int j = 0; j < Left.size(); j++)
		{
			double t=1.0 - im::distance(Left[i], Left[j]);
			double t1=im::distance(Left[i], Left[j]);
			//L(i,j) = 1.0 / std::exp(im::distance(Left[i], Left[j]));
			//L(i,j) = t;
			L(i,j) = t*t;
			//L(i,j) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1+t1*t1*t1*t1*t1);
		}
	algebraVector data(Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
		data(i)=Left[i]->data();

	ls::dpcg solver;

	algebraVector res =	solver.solve(L, data);

	algebraMatrix M(Right.size(), Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Right.size(); i++)
		for (unsigned int j = 0; j < Left.size(); j++)
		{
			double t=1.0 - im::distance(Left[i], Left[j]);
			double t1=im::distance(Left[i], Left[j]);
			//M(i,j) = t;
			M(i,j) = t*t;
			//M(i,j) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1+t1*t1*t1*t1*t1);
			//M(i, j) = 1.0 - im::distance(Left[i], Left[j]);
			//M(i, j) = 1.0 / std::exp(im::distance(Left[i], Left[j]));
		}
	algebraVector Res = M * res;

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Right.size(); i++)
		Right[i]->data(Res(i));

	return Right;
}

std::vector<node*> rbf::deformation(std::vector<node*> Left, std::vector<node*> Right)
{
	algebraMatrix M(Left.size(), Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
		for (unsigned int j = 0; j < Left.size(); j++)
		{
			double t=1.0 - im::distance(Left[i], Left[j]);
			double t1=im::distance(Left[i], Left[j]);
			//M(i,j) = t;
			M(i,j) = t*t;
			//M(i,j) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1+t1*t1*t1*t1*t1);
			//M(i, j) = 1.0 - im::distance(Left[i], Left[j]);
			//M(i, j) = 1.0 / std::exp(im::distance(Left[i], Left[j]));

		}
	algebraVector dx(Left.size());
	algebraVector dy(Left.size());
	algebraVector dz(Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
	{
		dx(i) = Left[i]->dx();
		dy(i) = Left[i]->dy();
		dz(i) = Left[i]->dz();
	}

	ls::dpcg solver;
	algebraVector res1 = solver.solve(M, dx);
	algebraVector res2 = solver.solve(M, dy);
	algebraVector res3 = solver.solve(M, dz);

	algebraMatrix K(Right.size(), Left.size());

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Right.size(); i++)
			for (unsigned int j = 0; j < Left.size(); j++)
			{
				double t=1.0 - im::distance(Left[i], Left[j]);
				double t1=im::distance(Left[i], Left[j]);
				//K(i,j) = t;
				K(i,j) = t*t;
				//K(i,j) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1+t1*t1*t1*t1*t1);
				//K(i, j) = 1.0 - im::distance(Left[i], Left[j]);
			//	K(i, j) = 1.0 / std::exp(im::distance(Left[i], Left[j]));

			}
	algebraVector Res1 = K * res1;
	algebraVector Res2 = K * res2;
	algebraVector Res3 = K * res3;

#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Right.size(); i++)
		Right[i]->dxdydz(Res1(i), Res2(i), Res3(i));

	return Right;
}

std::vector<std::vector<node*> > rbf::deformation(std::vector<node*> Left, std::vector<std::vector<node*> > Right)
{
	const unsigned int dim = Right.size();

	algebraMatrix M(Left.size(), Left.size());
#pragma omp parallel for num_threads(num_threads)
	for (unsigned int i = 0; i < Left.size(); i++)
		for (unsigned int j = 0; j < Left.size(); j++)
		{
			double t=1.0 - im::distance(Left[i], Left[j]);
			double t1=im::distance(Left[i], Left[j]);
			M(i,j) = t;
//			M(i,j) = t*t;
			//M(i,j) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1+t1*t1*t1*t1*t1);
			//M(i, j) = 1.0 - im::distance(Left[i], Left[j]);
			//M(i, j) = 1.0 / std::exp(im::distance(Left[i], Left[j]));
		}
	algebraVector* d = new algebraVector[dim];
	for (unsigned int i = 0; i < dim; i++)
		d[i].Resize(Left.size());

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Left.size(); j++)
			d[i](j) = Left[j]->di(i);

	algebraVector *res = new algebraVector[dim];
	for (unsigned int i = 0; i < dim; i++)
		res[i].Resize(Left.size());

	ls::dpcg solver;
	for (unsigned int i = 0; i < dim; i++)
		res[i] = solver.solve(M, d[i]);

	algebraMatrix* K = new algebraMatrix[dim];
	for (unsigned int i = 0; i < dim; i++)
		K[i].Resize(Right[i].size(), Left.size());

	for ( unsigned int i = 0; i < dim; i++ )
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Right[i].size(); j++)
			for (unsigned int k = 0; k < Left.size(); k++)
			{
					double t=1.0 - im::distance(Left[i], Left[j]);
//					double t1=im::distance(Left[i], Left[j]);
//					K[i](j, k) = t;
//					K[i](j, k) = t*t;
					//K[i](j, k) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1+t1*t1*t1*t1*t1);
					//K[i](j, k) = 1.0 - im::distance(Left[i], Left[j]);
					K[i](j, k) = 1.0 / std::exp(im::distance(Left[i], Left[j]));

			}

	algebraVector *Res = new algebraVector[dim];
	for (unsigned int i = 0; i < dim; i++)
		Res[i].Resize(Right[i].size());

	for (unsigned int i = 0; i < dim; i++)
		Res[i] = K[i] * res[i];

	for (unsigned int i = 0; i < dim; i++)
		for (unsigned int j = 0; j < Right[i].size(); j++)
			Right[i][j]->dxdydz(i, Res[i](j));

	return Right;
}

std::vector<std::vector<node*> > rbf::deformation(std::vector<std::vector<mesh::node*> > Left, std::vector<std::vector<node*> > Right)
{
	const unsigned int dim = (Right.size() == Left.size()) ? Right.size() : 0;

	algebraMatrix* M = new algebraMatrix[dim];
	for (unsigned int i = 0; i < dim; i++)
		M[i].Resize(Left[i].size(), Left[i].size());

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Left[i].size(); j++)
			for (unsigned int k = 0; k < Left[i].size(); k++)
			{

//						double t = 1.0 - im::distance(Left[i][j], Left[i][k]);
//						double t1 = im::distance(Left[i][j], Left[i][k]);
//						M[i](j, k) = t;
//						M[i](j, k) = t * t;
//						M[i](j, k) = t * t * t * ( 1 + 3 * t1 * t1 * t1);
//						M[i](j, k) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1 + t1*t1*t1*t1);
						M[i](j, k) = 1.0 / std::exp(im::distance(Left[i][j], Left[i][k]));
			}

	algebraVector* d = new algebraVector[dim];
	for (unsigned int i = 0; i < dim; i++)
		d[i].Resize(Left[i].size());

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Left[i].size(); j++)
			d[i](j) = Left[i][j]->di(i);

	algebraVector *res = new algebraVector[dim];
	for (unsigned int i = 0; i < dim; i++)
		res[i].Resize(Left[i].size());

	ls::dpcg solver;
	for (unsigned int i = 0; i < dim; i++)
		res[i] = solver.solve(M[i], d[i]);

	algebraMatrix* K = new algebraMatrix[dim];
	for (unsigned int i = 0; i < dim; i++)
		K[i].Resize(Right[i].size(), Left[i].size());

	for ( unsigned int i = 0; i < dim; i++ )
#pragma omp parallel for num_threads(num_threads)
		for (unsigned int j = 0; j < Right[i].size(); j++)
			for (unsigned int k = 0; k < Left[i].size(); k++)
			{

//						double t = 1.0 - im::distance(Right[i][j], Left[i][k]);
//						double t1 = im::distance(Right[i][j], Left[i][k]);
//						K[i](j, k) = t;
//						K[i](j, k) = t * t;
//						K[i](j, k) = t * t * t * ( 1 + 3 * t1 * t1 * t1);
//						K[i](j, k) = t*t*t*t*t*(1+5*t1+9*t1*t1+5*t1*t1*t1 + t1*t1*t1*t1);
						K[i](j, k) = 1.0 / std::exp(im::distance(Right[i][j], Left[i][k]));
			}

	for (unsigned int i = 0; i < dim; i++)
#pragma omp parallel for num_threads(num_threads)
		for (unsigned j = 0; j < K[i].Rows(); j++)
			Right[i][j]->dxdydz(i, K[i].Row(j) * res[i]);

	return Right;
}

} /* namespace im */
