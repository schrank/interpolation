#include "tfi.h"
#include "../algebra/algebraMatrix.h"
#include "../linearSolver/dpcg.h"

#include <cmath>
#include <fstream>

using namespace mesh;
using namespace algebra;
using namespace std;

namespace im {

tfi::tfi() {
	// TODO Auto-generated constructor stub
}

tfi::~tfi() {
	// TODO Auto-generated destructor stub
}


vector<node*> tfi::interpolate(vector<node*> Left, vector<node*> Right)
{
	algebraMatrix xi(Left.size(), Left.size());
	algebraMatrix eta(Left.size(), Left.size());
	algebraMatrix zeta(Left.size(), Left.size());

	algebraVector d(3);

	algebraMatrix F1(Left.size(), Left.size());
	algebraMatrix F2(Left.size(), Left.size());
	algebraMatrix F3(Left.size(), Left.size());
	algebraMatrix F1F2(Left.size(), Left.size());
	algebraMatrix F2F3(Left.size(), Left.size());
	algebraMatrix F3F1(Left.size(), Left.size());
	algebraMatrix F1F2F3(Left.size(), Left.size());


	algebraVector dx(Left.size());
		algebraVector dy(Left.size());
		algebraVector dz(Left.size());

		for (unsigned int i = 0; i < Left.size(); i++)
		{
			dx(i) = Left[i]->dx();
			dy(i) = Left[i]->dy();
			dz(i) = Left[i]->dz();
		}

/*
	unsigned int I=2,J=2,K=2;
	for (unsigned int i = 1; i < I; i++)
			for (unsigned int j = 1; j < J; j++)
				for (unsigned int k = 1; k < K; k++)
			{
					xi(1,j,k)=0;
					eta(i,1,k)=0;
					zeta(i,j,1)=0;

					for (unsigned int n = 2; n < i; n++)

				xi(i,j,k)=std::exp(im::distance(Left[i], Left[j]));


				eta(i,j,k)=std::exp(im::distance(Left[i], Left[j]));
				zeta(i,j,k)=std::exp(im::distance(Left[i], Left[j]));
			}

	for (unsigned int i = 1; i < I; i++)
			for (unsigned int j = 1; j < J; j++)
				for (unsigned int k = 1; k < K; k++)
				{
					d(0)=dx(i,j,k);
					d(1)=dy(i,j,k);
					d(2)=dz(i,j,k);
				}


	for (unsigned int i = 1; i < I; i++)
		for (unsigned int j = 1; j < J; j++)
			for (unsigned int k = 1; k < K; k++)
			{
				F1(i,j,k)=(1-xi(i,j,k))*d(1,j,k)+xi(i,j,k)*d(I,j,k);
				F2(i,j,k)=(1-eta(i,j,k))*d(i,1,k)+eta(i,j,k)*d(i,J,k);
				F3(i,j,k)=(1-zeta(i,j,k))*d(i,j,1)+zeta(i,j,k)*d(i,j,K);
				F1F2(i,j,k)=F1(i,j,k)*F2(i,j,k);
				F2F3(i,j,k)=F2(i,j,k)*F3(i,j,k);
				F3F1(i,j,k)=F3(i,j,k)*F1(i,j,k);
				F1F2F3(i,j,k)=F1(i,j,k)*F2(i,j,k)*F3(i,j,k);

				d(i,j,k)=F1+F2+F3-F1F2-F2F3-F3F1+F1F2F3;

			}
*/
		return vector<node*>();
	}
} /* namespace im */
